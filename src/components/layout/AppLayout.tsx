import React, { useEffect, useMemo, useState } from 'react'
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined
} from '@ant-design/icons'
import { Layout, Menu, Button } from 'antd'
import { Outlet, useNavigate } from 'react-router-dom'
import './styles.scss'
import { Resizable } from 're-resizable'
import { reverse } from 'lodash'
import { navigationItems } from '../navigation/Navigation.tsx'
import NavProfile from '../../pages/auth/profile/NavProfile.tsx'
import { useAppSelector } from '../../app/hooks/hook.ts'
import { appName } from '../../app/constants/appConstant.ts'

const { Header, Sider, Content } = Layout

const AppLayout = () => {
  const accessToken = localStorage.getItem(appName)
  const [collapsed, setCollapsed] = useState(false)
  const navigate = useNavigate()
  const handleSelectMenu = (event: any) => {
    const keys: string[] = event.keyPath
    const format = keys.map((r) => {
      if (!r.includes('/')) {
        return r
      } else {
        return r.replaceAll('/', '')
      }
    })
    const path = `/${reverse(format).join('/')}`
    navigate(path)
  }
  useEffect(() => {
    if (!accessToken) {
      navigate('/auth/login')
    } else if (accessToken && location.pathname === '/') {
      navigate('/system-params/users')
    }
  }, [accessToken, navigate])

  const currentKey = useMemo(() => {
    const selected = location.pathname.split('/')
    return selected.filter((n) => n !== '')
  }, [location.pathname])

  const enable = {
    top: false,
    right: true,
    bottom: false,
    left: false,
    topRight: false,
    bottomRight: false,
    bottomLeft: false,
    topLeft: false
  }

  return (
    <Layout className='app-layout' style={{ height: '100vh' }}>
      <Resizable
        maxWidth={collapsed ? '80px' : '300px'}
        minWidth={collapsed ? '80px' : '200px'}
        className='resizable'
        enable={enable}
      >
        <Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
          className='custom-sidebar'
          theme='light'
        >
          <div className='logo'>
            CMS
          </div>
          <Menu
            className='scroll-custom sidebar-menu'
            mode='inline'
            onClick={handleSelectMenu}
            selectedKeys={currentKey}
            defaultOpenKeys={currentKey}
            items={navigationItems}
          />
        </Sider>
      </Resizable>
      <Layout className='site-layout'>
        <Header
          className='app-header'
        >
          <Button
            type='text'
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: '16px',
              width: 64,
              height: 64
            }}
          />
          <NavProfile />
        </Header>
        <Content
          className='app-content'
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  )
}

export default AppLayout