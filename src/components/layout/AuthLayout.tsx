import {Outlet, useLocation, useNavigate} from 'react-router-dom'
import {useEffect} from "react";
import { appName } from '../../app/constants/appConstant.ts'

const AuthLayout = () => {
    const accessToken = localStorage.getItem(appName)
    const navigate = useNavigate()
    const location = useLocation()

    useEffect(() => {
        if (location.pathname === '/') {
            navigate('/auth/login')
        }
        if (accessToken) {
            navigate('/system-params/users')
        }
    }, [navigate, accessToken])

    return (
        <main className='main-content'>
            <Outlet/>
        </main>
    )
}

export default AuthLayout
