import { ItemType } from 'antd/es/menu/hooks/useItems'
import {
  PushpinOutlined,
  LineChartOutlined,
  HomeOutlined,
  TransactionOutlined,
  TeamOutlined,
  DollarOutlined,
  FileDoneOutlined,
  ShoppingCartOutlined,
  AppstoreOutlined
} from '@ant-design/icons'

export type ItemTypeCustom = ItemType & {
  link?: string
  children?: ItemTypeCustom[]
}
const systemParamsManagement = {
  key: 'system-params',
  icon: <HomeOutlined />,
  label: 'Tham số hệ thống',
  children: [
    {
      key: 'merchants',
      icon: <PushpinOutlined />,
      label: 'Danh sách merchant'
    },
    {
      key: 'users',
      icon: <PushpinOutlined />,
      label: 'Danh sách người dùng'
    },
    {
      key: 'customers',
      icon: <PushpinOutlined />,
      label: 'Danh sách khách hàng'
    }
  ]
}

const merchantTransactionManagement = {
  key: 'merchant-transaction-management',
  icon: <TransactionOutlined />,
  label: 'Quản lý TTGD merchant'
}

const cashFlowManagement = {
  key: 'cash-flow-management',
  icon: <DollarOutlined />,
  label: 'Quản lý dòng tiền'
}

const investmentManagement = {
  key: 'investment-management',
  icon: <LineChartOutlined />,
  label: 'Quản lý DM đầu tư'
}

const termManagement = {
  key: 'term-management',
  icon: <FileDoneOutlined />,
  label: 'Quản lý điều khoản đăng ký'
}

const productManagement = {
  key: 'product-management',
  icon: <ShoppingCartOutlined />,
  label: 'Quản lý sản phẩm'
}

const itemManagement = {
  key: 'item-management',
  icon: <AppstoreOutlined />,
  label: 'Quản lý mặt hàng'
}

const tradeHistory = {
  key: 'trade-history',
  icon: <TransactionOutlined />,
  label: 'Lịch sử giao dịch'
}

const interestRate = {
  key: 'interest-rate',
  icon: <DollarOutlined />,
  label: 'Tỷ lệ lãi suất'
}

export const navigationItems: ItemTypeCustom[] = [
  systemParamsManagement,
  // merchantTransactionManagement,
  // customerManagement,
  // cashFlowManagement,
  // investmentManagement,
  termManagement,
  productManagement,
  itemManagement,
  tradeHistory,
  interestRate
]