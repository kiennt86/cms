import {
    ExclamationCircleOutlined,
    LogoutOutlined,
} from '@ant-design/icons'
import {Avatar, Col, Dropdown, MenuProps, Modal, Row, Typography} from 'antd'
import React from 'react'
import { useAppDispatch, useAppSelector } from '../../../app/hooks/hook.ts'
import {logout} from "../../../app/slices/authSlice.ts";
import {authService} from "../../../app/services/apiAuthService.ts";
import { useNavigate } from 'react-router-dom'

const {Text} = Typography

export default function NavProfile() {
    const router = useNavigate()
    const { profile } = useAppSelector((state) => state.auth)
    const dispatch = useAppDispatch()
    const items: MenuProps['items'] = [
        {
            label: 'Đăng xuất',
            icon: <LogoutOutlined/>,
            key: 'logout'
        }
    ]

    const handleMenuClick: MenuProps['onClick'] = (event) => {
        switch (event.key) {
            case 'logout':
                dispatch(logout())
                router("/auth/login")
                break
        }
    }

    const menuProps = {
        items,
        onClick: handleMenuClick
    }

    const handleSignout = async () => {
        dispatch(authService.util.resetApiState())
        dispatch(logout())
    }

    const signOut = () => {
        Modal.confirm({
            title: 'Đăng xuất',
            icon: <ExclamationCircleOutlined/>,
            content: 'Bạn có muốn đăng xuất khỏi tài khoản này không?',
            okText: 'Đăng xuất',
            cancelText: 'Hủy',
            onOk: handleSignout
        })
    }

    return (
        <React.Fragment>
            <Dropdown
                placement='bottom'
                menu={menuProps}
                trigger={['click']}
                arrow={{pointAtCenter: true}}
            >
                <Row gutter={16}>
                    <Col>
                        <Text className='fw-500'>{profile?.name}</Text>
                    </Col>
                    <Col>
                        <Avatar size='large'>CMS</Avatar>
                    </Col>
                </Row>
            </Dropdown>
        </React.Fragment>
    )
}
