import {Col, Row} from 'antd'

interface IProps {
    title?: string
    description?: string
    children: any
}

const AuthenticationLayout = ({title, description, children}: IProps) => {
    return (
        <article id='login-page'>
            <Row className='w-100 h-100'>
                <Col
                    xs={0}
                    sm={0}
                    md={0}
                    lg={11}
                    xl={10}
                >
                    <div className='login-bg-mask login-mask-color'/>
                    <div className='login-bg'/>
                    <div className='login-description h-100 d-flex'>
                        <div className='m-auto'>
                            {title && <h1 className='t-white f-30'>{title}</h1>}
                            {description && <p className='t-white'>{description}</p>}
                        </div>
                    </div>
                </Col>
                <Col
                    xs={20}
                    sm={20}
                    md={24}
                    lg={13}
                    xl={14}
                >
                    <div className='d-flex flex-column justify-center h-100'>
                        <Row justify='center'>
                            <Col
                                xs={24}
                                sm={24}
                                md={20}
                                lg={16}
                                xl={12}
                            >
                                {children}
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        </article>
    )
}
export default AuthenticationLayout
