import {Button, Col, Form, Input, Row} from 'antd'
import {useForm} from 'antd/es/form/Form'
import {useNavigate} from 'react-router-dom'
import {MailOutlined} from '@ant-design/icons'
import './styles.scss'
import AuthenticationLayout from '../authentication-layout/AuthenticationLayout'

function ForgotPassword() {
    const navigate = useNavigate()
    const [form] = useForm()

    const handleCancel = () => {
        navigate('/auth/login')
    }

    return (
        <AuthenticationLayout title='Quên mật khẩu'>
            <h1 className='mb-10'>Quên mật khẩu</h1>
            <span className='d-block text-italic mb-30'>Để lấy lại mật khẩu đăng nhập/link kích hoạt tài khoản, bạn nhập chính xác địa chỉ email đã khai báo vào form dưới đây. Hệ thống sẽ gửi một đường link kích hoạt tới email của bạn để xác thực yêu cầu.</span>
            <Form
                layout='vertical'
                // onFinish={handleSubmit}
                form={form}
            >
                <Form.Item
                    label=''
                    name='email'
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng cung cấp email'
                        },
                        {
                            pattern: new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
                            message: 'Vui lòng nhập đúng định dạng email'
                        }
                    ]}
                >
                    <Input
                        prefix={<MailOutlined className='site-form-item-icon'/>}
                        placeholder='Vui lòng nhập email'
                    />
                </Form.Item>
                <Row
                    gutter={10}
                    justify='end'
                    className='mt-10'
                >
                    <Col>
                        <Form.Item>
                            <Button
                                onClick={handleCancel}
                                block
                                style={{width: '100px'}}
                            >
                                Hủy
                            </Button>
                        </Form.Item>
                    </Col>
                    <Col>
                        <Form.Item>
                            <Button
                                danger
                                htmlType='submit'
                                type='primary'
                                style={{width: '100px'}}
                            >
                                Gửi
                            </Button>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </AuthenticationLayout>
    )
}

export default ForgotPassword
