import { Button, Col, Form, Input, Row } from 'antd'
import { Link, useNavigate } from 'react-router-dom'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import './styles.scss'
import { useGoogleLogin } from '@react-oauth/google'
import axios from 'axios'
import { appName } from '../../../app/constants/appConstant.ts'
import { useDispatch } from 'react-redux'
import { getProfile } from '../../../app/slices/authSlice.ts'

function Login() {
  const dispatch = useDispatch()
  const navigate = useNavigate()


  const loginGoogle = useGoogleLogin({
    onSuccess: async (response) => {
      try {
        const res = await axios.get(
          'https://www.googleapis.com/oauth2/v3/userinfo',
          {
            headers: {
              Authorization: `Bearer ${response.access_token}`
            }
          }
        )
        dispatch(getProfile(res.data))
        localStorage.setItem(appName, response.access_token)
        navigate('/system-params/users')
      } catch (e) {
        console.log(e)
      }
    }
  })

  return (
    <article id='login-page'>
      <Row className='w-100 h-100'>
        <Col
          xs={0}
          sm={0}
          md={0}
          lg={11}
          xl={10}
        >
          <div className='login-bg-mask login-mask-color' />
          <div className='login-bg' />
          <div className='login-description h-100 d-flex'>
            <div className='m-auto'>
              <h1 className='t-white f-30'>Đăng nhập</h1>
              <p className='t-white'>Đăng nhập để sử dụng những tính năng ưu việt</p>
            </div>
          </div>
        </Col>
        <Col
          xs={20}
          sm={20}
          md={24}
          lg={13}
          xl={14}
        >
          <div className='d-flex flex-column justify-center h-100'>
            <Row justify='center'>
              <Col
                xs={24}
                sm={24}
                md={20}
                lg={12}
                xl={10}
              >
                <h1 className='mb-30'>Đăng nhập</h1>
                <Form
                  name='normal_login'
                  initialValues={{
                    email: '',
                    password: ''
                  }}
                  // onFinish={onFinish}
                >
                  <Form.Item
                    name='email'
                    rules={[
                      {
                        required: true,
                        message: 'Bạn chưa nhập tài khoản'
                      }
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined className='site-form-item-icon' />}
                      placeholder='Nhập email'
                    />
                  </Form.Item>
                  <Form.Item
                    name='password'
                    rules={[
                      {
                        required: true,
                        message: 'Bạn chưa nhập mật khẩu'
                      }
                    ]}
                  >
                    <Input
                      prefix={<LockOutlined className='site-form-item-icon' />}
                      type='password'
                      placeholder='Nhập mật khẩu'
                    />
                  </Form.Item>
                  <Row>
                    <Col
                      xs={24}
                      md={12}
                      offset={12}
                    >
                      <Form.Item className='text-end'>
                        <Link
                          className='mb-24'
                          to='/auth/forget-password'
                        >
                          Quên mật khẩu
                        </Link>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Form.Item
                    className='text-center'
                    noStyle
                  >
                    <Button
                      danger
                      type='primary'
                      htmlType='submit'
                      block
                      // loading={isLoading}
                    >
                      <b>Đăng nhập</b>
                    </Button>
                    <Button
                      className='mt-10'
                      danger
                      type='primary'
                      htmlType='submit'
                      block
                      onClick={() => loginGoogle()}
                    >
                      <b>Đăng nhập bằng Gmail</b>
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </article>
  )
}

export default Login
