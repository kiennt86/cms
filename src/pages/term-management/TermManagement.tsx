import {
  PlusOutlined,
  SearchOutlined,
  EditOutlined,
  DeleteOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Table, Tag, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useMemo, useState } from 'react'
import dayjs from 'dayjs'
import { ApiCode, viewMode } from '../../app/types/api.ts'
import { getStatusText, getTagColor } from '../../app/constants/helperFunctions.ts'
import { REQUEST_ID } from '../../app/constants/appConstant.ts'
import { useGetListTermsMutation } from '../../app/services/apiTermService.ts'
import { TermInterface, TermSearchParams } from '../../app/types/term.ts'
import { TermModal } from './term-modal/TermModal.tsx'

export const TermManagement = () => {
  const { Text } = Typography
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'
  const [getTermListMutation] = useGetListTermsMutation()
  const [termData, setTermData] = useState<TermInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<TermInterface>()
  const [form] = Form.useForm()
  const [search, setSearch] = useState<TermSearchParams>({})

  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  const fetchData = async () => {
    try {
      const body = {
        apiCode: ApiCode.TERMS_GET,
        requestId: REQUEST_ID,
        params: search
      }

      const terms: any = await getTermListMutation(body)
      setTermData(terms?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [search])

  const termList = useMemo(() => {
    if (termData && termData?.length > 0) {
      return termData?.map((item: TermInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [termData])


  const handleModal = (action: viewMode, record?: TermInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }

  const handleSearch = async (values: TermSearchParams) => {
    const query = {
      ...values,
      code: values.code === '' ? undefined : values.code?.trim()
    }
    setSearch(query)
  }


  const columns: ColumnsType<TermInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã số',
      dataIndex: 'code',
      key: 'code',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tiêu đề',
      dataIndex: 'title',
      key: 'title',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }

    },
    {
      title: 'Loại',
      dataIndex: 'type',
      key: 'type',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mô tả',
      key: 'desc',
      dataIndex: 'desc',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Thời gian tạo',
      key: 'timeCreate',
      dataIndex: 'timeCreate',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'Thời gian cập nhật',
      key: 'updateTime',
      dataIndex: 'updateTime',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'Trạng thái',
      key: 'status',
      dataIndex: 'status',
      align: 'center',
      render: (record: string) => (
        <Tag
          color={getTagColor(record)}
          className='text-capitalize'
        >
          {getStatusText(record)}
        </Tag>
      )
    },
    {
      title: 'Tác vụ',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (value, record) => (
        <Row
          gutter={5}
          justify='center'
        >
          <Col>
            <Tooltip
              placement='top'
              title='Sửa'
            >
              <Button
                icon={<EditOutlined />}
                onClick={() => {
                  handleModal(viewMode.update, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
          <Col>
            <Tooltip
              placement='top'
              title='Xóa'
            >
              <Button
                type='primary'
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  handleModal(viewMode.delete, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
        </Row>
      )
    }
  ]

  return (
    <Card title='Danh sách điều khoản đăng ký'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
          className='header'
        >
          <Form
            className='w-100'
            layout='vertical'
            onFinish={handleSearch}
            form={form}
            autoComplete='off'
          >
            <Row gutter={16}>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='code'
                  label='Mã điều khoản'
                >
                  <Input
                    allowClear
                    placeholder='Nhập điều khoản'
                    onChange={(e) => {
                      if (e.target.value === '' || e.target.value === undefined) {
                        setSearch({
                          ...search,
                          code: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col className='mt-30'>
                <Button
                  danger
                  type='primary'
                  htmlType='submit'
                  icon={<SearchOutlined />}
                >
                  Truy vấn
                </Button>
              </Col>
              <Col className='mt-30'>
                <Button
                  icon={<PlusOutlined />}
                  onClick={() => {
                    handleModal(viewMode.create)
                  }}
                >
                  Thêm mới
                </Button>
              </Col>
            </Row>
          </Form>
          <Divider />
        </Col>
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={termList}
                 scroll={{ x: '100%' }}
                 rowKey={'no'}
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
      <TermModal
        mode={mode}
        visible={visible}
        onCancel={onCancel}
        record={selectedRecord}
        fetchData={fetchData}
      />
    </Card>
  )
}

