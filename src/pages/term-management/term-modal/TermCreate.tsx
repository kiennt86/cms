import { Button, Col, Form, Input, Modal, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useCreateTermMutation } from '../../../app/services/apiTermService.ts'


interface IProps {
  onCancel: () => void
  fetchData: () => void
}

interface TermCreate {
  code: string,
  title: string,
  type: string,
  desc: string,
  status: string
}

export const TermCreate = ({ onCancel, fetchData }: IProps) => {
  const [termCreate, { isSuccess }] = useCreateTermMutation()
  const [form] = Form.useForm()
  const handleCreate = async (value: TermCreate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.TERMS_CREATE,
      requestId: REQUEST_ID,
      params: {
        code: value.code,
        title: value.title,
        type: value.type,
        desc: value.desc,
        status: value.status
      }
    }
    const result: any = await termCreate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Tạo mới điều khoản thành công' })
      onCancel()
      form.resetFields()
      fetchData()
    } else if (result?.data?.msgRepose == 'Terms exits.') {
      notification.error({ message: 'Điều khoản đã tồn tại' })
    } else {
      notification.error({ message: 'Tạo mới điều khoản thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleCreate}
      autoComplete='off'
      layout='vertical'
      initialValues={{
        status: 'ACTIVE'
      }}
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã điều khoản'
            name='code'
            rules={[{ required: true, message: 'Hãy nhập mã điều khoản!' }]}
          >
            <Input
              placeholder='Nhập mã điều khoản'
            />
          </Form.Item>

          <Form.Item
            label='Tiêu đề'
            name='title'
            rules={[
              {
                required: true,
                message: 'Hãy nhập tiêu đề!'
              }
            ]}>
            <Input
              placeholder='Nhập tiêu đề'
            />
          </Form.Item>
          <Form.Item
            label='Loại điều khoản'
            name='type'
            rules={[{ required: true, message: 'Hãy nhập loại điều khoản!' }]}
          >
            <Input
              placeholder='Nhập loại điều khoản'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mô tả'
            name='desc'
          >
            <Input
              placeholder='Nhập mô tả'
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='status'
          >
            <Select
              options={[
                { value: 'ACTIVE', label: 'Hoạt động' },
                { value: 'INACTIVE', label: 'Không hoạt động' }
              ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Tạo mới
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

