import {Button, notification, Typography} from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { TermInterface } from '../../../app/types/term.ts'
import { useDeleteTermMutation } from '../../../app/services/apiTermService.ts'

const {Text} = Typography

type DeleteConfirmModalType = {
  fetchData: () => void
  onCancel: () => void
  record: TermInterface
}

export const TermDelete = ({
                                 onCancel,
                                 record,
                                 fetchData
                               }: DeleteConfirmModalType) => {

  const [deleteTerm] = useDeleteTermMutation()

  const onDelete = async () => {
    const body: BodyInterface = {
      apiCode: ApiCode.TERMS_DELETE,
      requestId: REQUEST_ID,
      params: {
        id: record.id,
      }
    }
    const result: any = await deleteTerm(body)
    if (result?.data?.code === '00') {
      notification.success({message: 'Xóa điều khoản thành công'})
      onCancel()
      fetchData()
    } else {
      notification.error({message: 'Xóa điều khoản thất bại'})
    }
  }

  return (
    <>
      <Text style={{textAlign: 'center'}}>
        Bạn có chắc chắn muốn xoá điều khoản này không?
      </Text>
      <div style={{textAlign: 'end', marginTop: '8px'}}>
        <Button
          className="mr-10"
          onClick={() => {
            onCancel()
          }}
        >
          Hủy
        </Button>
        <Button
          type="primary"
          onClick={onDelete}
          danger
        >
          Xóa
        </Button>
      </div>
    </>
  )
}
