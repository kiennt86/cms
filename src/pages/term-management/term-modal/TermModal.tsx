import { Modal } from 'antd'
import { viewMode } from '../../../app/types/api.ts'
import { TermDelete } from './TermDelete.tsx'
import { TermCreate } from './TermCreate.tsx'
import { TermUpdate } from './TermUpdate.tsx'
import { TermInterface } from '../../../app/types/term.ts'


interface IProps {
  mode: viewMode
  visible: boolean
  record: TermInterface
  onCancel: () => void
  fetchData: () => void
}

export const TermModal = ({
                            onCancel,
                            visible,
                            fetchData,
                            record,
                            mode,
                            ...props
                          }: IProps) => {

  const render = () => {
    switch (mode) {
      case viewMode.update:
        return (
          <TermUpdate
            onCancel={onCancel}
            fetchData={fetchData}
            record={record}
          />
        )
      case viewMode.create:
        return (
          <TermCreate
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
      case viewMode.delete:
        return (
          <TermDelete
            record={record}
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
    }
  }

  return (
    <Modal
      title={mode === viewMode.create ? 'Tạo mới điều khoản' : mode === viewMode.update ? 'Cập nhật điều khoản' : 'Xoá điều khoản'}
      open={visible}
      footer={false}
      onCancel={onCancel}
      centered
      destroyOnClose={true}
      width={
        mode === viewMode.update ||
        mode === viewMode.create ||
        mode === viewMode.view
          ? 800 : 600
      }
      maskClosable={false}
      mask
      {...props}
    >
      {render()}
    </Modal>
  )
}
