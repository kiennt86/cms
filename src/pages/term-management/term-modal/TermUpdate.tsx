import { Button, Col, Form, Input, notification, Row, Select } from 'antd'
import { useEffect } from 'react'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useUpdateTermMutation } from '../../../app/services/apiTermService.ts'


interface IProps {
  record: any
  onCancel: () => void
  fetchData: () => void
}

interface TermUpdate {
  code: string,
  title: string,
  type: string,
  desc: string,
  status: number
}

export const TermUpdate = ({ onCancel, fetchData, record }: IProps) => {
  const [termUpdate, { isSuccess }] = useUpdateTermMutation()
  const [form] = Form.useForm()

  useEffect(() => {
    if (record) {
      form.setFieldsValue({
        code: record.code,
        title: record.title,
        type: record.type,
        desc: record.desc,
        status: record.status
      })
    }
  }, [record])

  const handleUpdate = async (value: TermUpdate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.TERMS_UPDATE,
      requestId: REQUEST_ID,
      params: {
        code: value.code,
        title: value.title,
        type: value.type,
        desc: value.desc,
        status: value.status
      }
    }
    const result: any = await termUpdate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Cập nhật điều khoản thành công' })
      onCancel()
      fetchData()
    } else {
      notification.error({ message: 'Cập nhật điều khoản thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleUpdate}
      autoComplete='off'
      layout='vertical'
      form={form}
    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã điều khoản'
            name='code'
            rules={[{ required: true, message: 'Hãy nhập mã điều khoản!' }]}
          >
            <Input
              disabled
              placeholder='Nhập mã điều khoản'
            />
          </Form.Item>

          <Form.Item
            label='Tiêu đề'
            name='title'
            rules={[
              {
                required: true,
                message: 'Hãy nhập tiêu đề!'
              }
            ]}>
            <Input
              placeholder='Nhập tiêu đề'
            />
          </Form.Item>
          <Form.Item
            label='Loại điều khoản'
            name='type'
            rules={[{ required: true, message: 'Hãy nhập loại điều khoản!' }]}
          >
            <Input
              placeholder='Nhập loại điều khoản'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mô tả'
            name='desc'
          >
            <Input
              placeholder='Nhập mô tả'
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='status'
          >
            <Select
              options={[
                { value: 'ACTIVE', label: 'Hoạt động' },
                { value: 'INACTIVE', label: 'Không hoạt động' }
              ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Cập nhật
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

