import {
  SearchOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Table, Tag, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useMemo, useState } from 'react'
import { useGetCustomerMutation } from '../../../app/services/apiCustomerService.ts'
import { CustomerInterface, CustomerSearchParams } from '../../../app/types/customer.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { ApiCode } from '../../../app/types/api.ts'
import { getStatusText, getTagColor } from '../../../app/constants/helperFunctions.ts'

export const CustomerList = () => {
  const { Text } = Typography
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'
  const [getCustomerListMutation] = useGetCustomerMutation()
  const [customerData, setCustomerData] = useState<CustomerInterface[]>([])
  const [form] = Form.useForm()
  const [search, setSearch] = useState<CustomerSearchParams>({})


  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  async function fetchData() {
    try {
      const body = {
        apiCode: ApiCode.CUSTOMER_GET,
        requestId: REQUEST_ID,
        params: search
      }
      const customer: any = await getCustomerListMutation(body)
      setCustomerData(customer?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  function wrapIntoArray(customerData) {
    const arr = []
    if (!Array.isArray(customerData)) {
      return [customerData]
    }
    return customerData
  }

  useEffect(() => {
    fetchData()
  }, [search])


  useEffect(() => {
    const result = wrapIntoArray(customerData)
    setCustomerData(result)
  }, [customerData])

  const customerList = useMemo(() => {
    if (customerData && customerData?.length > 0) {
      return customerData?.map((item: CustomerInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [customerData])


  const handleSearch = (values: CustomerSearchParams) => {
    const query = {
      ...values,
      phone: values.phone?.trim()
    }
    const newQuery = {}
    for (const key in query) {
      if (query[key] && query[key] !== undefined && query[key] !== null) {
        newQuery[key] = query[key]
      }
    }
    setSearch(newQuery)
  }

  const columns: ColumnsType<CustomerInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên',
      dataIndex: 'firstname',
      key: 'firstname',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Họ',
      dataIndex: 'lastname',
      key: 'lastname',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }

    },
    {
      title: 'CCCD',
      dataIndex: 'cccd',
      key: 'cccd',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số điện thoại',
      key: 'phone',
      dataIndex: 'phone',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Email',
      key: 'email',
      dataIndex: 'email',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Địa chỉ',
      key: 'address',
      dataIndex: 'address',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tài khoản ảo',
      key: 'va',
      dataIndex: 'va',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số tài khoản ngân hàng',
      key: 'bankAcc',
      dataIndex: 'bankAcc',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'ID đối tác',
      key: 'partnerId',
      dataIndex: 'partnerId',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tài khoản giám sát',
      key: 'custAccount',
      dataIndex: 'custAccount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Giá trị',
      key: 'amount',
      dataIndex: 'amount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Trạng thái',
      key: 'status',
      dataIndex: 'status',
      align: 'center',
      render: (record: boolean | string | number) => (
        <Tag
          color={getTagColor(record)}
          className='text-capitalize'
        >
          {getStatusText(record)}
        </Tag>
      )
    }
  ]

  return (
    <Card title='Danh sách khách hàng'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
          className='header'
        >
          <Form
            className='w-100'
            layout='vertical'
            onFinish={handleSearch}
            form={form}
            autoComplete='off'
          >
            <Row gutter={16}>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='phone'
                  label='Số điện thoại'
                >
                  <Input
                    allowClear
                    placeholder='Hãy nhập số điện thoại'
                    onChange={(e) => {
                      if (e.target.value === '' || e.target.value === undefined) {
                        setSearch({
                          ...search,
                          phone: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col className='mt-30'>
                <Button
                  danger
                  type='primary'
                  htmlType='submit'
                  icon={<SearchOutlined />}
                >
                  Truy vấn
                </Button>
              </Col>
            </Row>
          </Form>
          <Divider />
        </Col>
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={customerList}
                 scroll={{ x: '100%' }}
                 rowKey='no'
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
    </Card>
  )
}

