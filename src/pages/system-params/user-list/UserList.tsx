import {
  DeleteOutlined,
  EditOutlined,
  SearchOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col, DatePicker,
  Divider,
  Form,
  Input,
  Row,
  Table, Tag, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useGetListUsersMutation } from '../../../app/services/apiUserService.ts'
import { ApiCode } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useEffect, useMemo, useState } from 'react'
import { viewMode } from '../../../app/types/merchant.ts'
import { UserInterface, UserSearchParams } from '../../../app/types/user.ts'
import dayjs from 'dayjs'
import { getStatusText, getTagColor } from '../../../app/constants/helperFunctions.ts'
import { UserModal } from './user-modal/UserModal.tsx'

export const UserList = () => {
  const { Text } = Typography
  const { RangePicker } = DatePicker
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'
  const [getUserListMutation] = useGetListUsersMutation()
  const [userData, setUserData] = useState<UserInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<UserInterface>()
  const [form] = Form.useForm()
  const [search, setSearch] = useState<UserSearchParams>({})
  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  async function fetchData() {
    try {
      const body = {
        apiCode: ApiCode.USER_GET,
        requestId: REQUEST_ID,
        params: search
      }
      const users: any = await getUserListMutation(body)
      setUserData(users?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [search])

  const userList = useMemo(() => {
    if (userData && userData?.length > 0) {
      return userData?.map((item: UserInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [userData])

  const handleModal = (action: viewMode, record?: UserInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }

  const handleSearch = (values: UserSearchParams) => {
    const query = {
      ...values,
      name: values.name === '' ? undefined : values.name?.trim(),
      fromDate: values.time === undefined || values.time === null ? undefined : new Date(dayjs(values.time[0]).startOf('day').toISOString()).getTime(),
      toDate: values.time === undefined || values.time === null ? undefined : new Date(dayjs(values.time[1]).endOf('day').toISOString()).getTime()
    }
    const newQuery = {}
    for (const key in query) {
      if (query[key] && query[key] !== undefined) {
        newQuery[key] = query[key]
      }
    }
    setSearch(newQuery)
  }

  const columns: ColumnsType<UserInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên người dùng',
      dataIndex: 'firstname',
      key: 'firstname',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Họ người dùng',
      dataIndex: 'lastname',
      key: 'lastname',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số điện thoại',
      key: 'phone',
      dataIndex: 'phone',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mật khẩu',
      key: 'pass',
      dataIndex: 'pass',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Email',
      key: 'email',
      dataIndex: 'email',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Thời gian tạo',
      key: 'timeCreate',
      dataIndex: 'timeCreate',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'Thời gian cập nhật',
      key: 'updateTime',
      dataIndex: 'updateTime',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'Trạng thái',
      key: 'status',
      dataIndex: 'status',
      align: 'center',
      render: (record: string | boolean | number) => (
        <Tag
          color={getTagColor(record)}
          className='text-capitalize'
        >
          {getStatusText(record)}
        </Tag>
      )
    },
    {
      title: 'Tác vụ',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (value, record: UserInterface) => (
        <Row
          gutter={5}
          justify='center'
        >
          <Col>
            <Tooltip
              placement='top'
              title='Sửa'
            >
              <Button
                onClick={() => {
                  handleModal(viewMode.update, record)
                }}
                icon={<EditOutlined />}
              >
              </Button>
            </Tooltip>
          </Col>
          <Col>
            <Tooltip
              placement='top'
              title='Xóa'
            >
              <Button
                onClick={() => {
                  handleModal(viewMode.delete, record)
                }}
                type='primary'
                danger
                icon={<DeleteOutlined />}
              >
              </Button>
            </Tooltip>
          </Col>
        </Row>
      )
    }

  ]

  return (
    <Card title='Danh sách người dùng'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
          className='header'
        >
          <Form
            className='w-100'
            layout='vertical'
            onFinish={handleSearch}
            form={form}
            autoComplete='off'
          >
            <Row gutter={16}>
              <Col
                xs={24}
                lg={8}
              >
                <Form.Item
                  name='name'
                  label='Họ người dùng'
                >
                  <Input
                    placeholder='Nhập họ người dùng'
                    allowClear
                    onChange={(e) => {
                      if (e.target.value === '' || e.target.value === undefined) {
                        setSearch({
                          ...search,
                          name: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                lg={8}
              >
                <Form.Item
                  name='time'
                  label='Ngày tạo'
                >
                  <RangePicker
                    allowClear
                    className='w-100'
                    format={'YYYY/MM/DD'}
                    onChange={(e) => {
                      if (e === null) {
                        setSearch({
                          ...search,
                          fromDate: undefined,
                          toDate: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col className='mt-30'>
                <Button
                  danger
                  type='primary'
                  htmlType='submit'
                  icon={<SearchOutlined />}
                >
                  Truy vấn
                </Button>
              </Col>
            </Row>
          </Form>
          <Divider />
        </Col>
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={userList}
                 scroll={{ x: '100%' }}
                 rowKey={'no'}
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
      <UserModal
        mode={mode}
        visible={visible}
        onCancel={onCancel}
        record={selectedRecord}
        fetchData={fetchData}
      />
    </Card>
  )
}

