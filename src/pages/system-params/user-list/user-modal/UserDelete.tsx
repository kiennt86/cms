import { Button, notification, Typography } from 'antd'
import { ApiCode, BodyInterface } from '../../../../app/types/api.ts'
import { REQUEST_ID } from '../../../../app/constants/appConstant.ts'
import { UserInterface } from '../../../../app/types/user.ts'
import { useDeleteUserMutation } from '../../../../app/services/apiUserService.ts'

const { Text } = Typography

type DeleteConfirmModalType = {
  fetchData: () => void
  onCancel: () => void
  record: UserInterface
}

export const UserDelete = ({
                             onCancel,
                             record,
                             fetchData
                           }: DeleteConfirmModalType) => {

  const [deleteUser] = useDeleteUserMutation()

  const onDelete = async () => {
    const body: BodyInterface = {
      apiCode: ApiCode.USER_DELETE,
      requestId: REQUEST_ID,
      params: {
        id: record.id
      }
    }
    const result: any = await deleteUser(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Xóa người dùng thành công' })
      onCancel()
      fetchData()
    } else {
      notification.error({ message: 'Xóa người dùng thất bại' })
    }
  }

  return (
    <>
      <Text style={{ textAlign: 'center' }}>
        Bạn có chắc chắn muốn xoá người dùng này không?
      </Text>
      <div style={{ textAlign: 'end', marginTop: '8px' }}>
        <Button
          className='mr-10'
          onClick={() => {
            onCancel()
          }}
        >
          Hủy
        </Button>
        <Button
          type='primary'
          onClick={onDelete}
          danger
        >
          Xóa
        </Button>
      </div>
    </>
  )
}
