import { Modal } from 'antd'
import { viewMode } from '../../../../app/types/merchant.ts'
import { UserDelete } from './UserDelete.tsx'
import { UserUpdate } from './UserUpdate.tsx'
import { UserInterface } from '../../../../app/types/user.ts'

interface IProps {
  mode: viewMode
  visible: boolean
  record: UserInterface
  onCancel: () => void
  fetchData: () => void
}

export const UserModal = ({
                                onCancel,
                                visible,
                                fetchData,
                                record,
                                mode,
                                ...props
                              }: IProps) => {

  const render = () => {
    switch (mode) {
      case viewMode.update:
        return (
          <UserUpdate
            onCancel={onCancel}
            fetchData={fetchData}
            record={record}
          />
        )
      // case viewMode.create:
      //   return (
      //     <UserCreate
      //       onCancel={onCancel}
      //       fetchData={fetchData}
      //     />
      //   )
      case viewMode.delete:
        return (
          <UserDelete
            record={record}
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
    }
  }

  return (
    <Modal
      title={mode === viewMode.create ? 'Tạo mới người dùng' : mode === viewMode.update ? 'Cập nhật người dùng' : 'Xoá người dùng'}
      open={visible}
      footer={false}
      onCancel={onCancel}
      centered
      destroyOnClose={true}
      width={
        mode === viewMode.update ||
        mode === viewMode.create ||
        mode === viewMode.view
          ? 800 : 600
      }
      maskClosable={false}
      mask
      {...props}
    >
      {render()}
    </Modal>
  )
}
