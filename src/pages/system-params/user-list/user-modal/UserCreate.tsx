import {Button, Col, Form, Input, Modal, notification, Row} from 'antd';
import {useCreateUserMutation} from "../../../../app/services/apiUserService.ts";
import {ApiCode, BodyInterface} from "../../../../app/types/api.ts";
import {REQUEST_ID} from "../../../../app/constants/appConstant.ts";


interface IProps {
    open: boolean,
    setOpen: (open: boolean) => void
}

interface UserCreate {
    firstName: string,
    lastName: string,
    phone: string,
    pass: string,
    status: string,
    email: string
}

export const UserCreate = ({open, setOpen}: IProps) => {

    const [userCreate] = useCreateUserMutation()

    const handleCreate = async (value: UserCreate) => {
        const body: BodyInterface = {
            apiCode: ApiCode.USER_CREATE,
            requestId: REQUEST_ID,
            params: {
                email: value.email,
                firstName: value.firstName,
                lastName: value.lastName,
                phone: value.phone,
                pass: value.pass,
                status: value.status
            }
        }
        const result: any = await userCreate(body)
        if (result?.data?.code === '00') {
            notification.success({ message: 'Tạo mới người dùng thành công' })
            setOpen(!open)
        } else {
            notification.error({ message: 'Tạo mới người dùng thất bại' })
        }
    }

    return (
        <Modal
            title="Tạo người dùng mới"
            centered
            open={open}
            onOk={() => setOpen(!open)}
            onCancel={() => setOpen(!open)}
            width={1000}
            footer={null}
        >
            <Form
                name="basic"
                initialValues={{remember: true}}
                onFinish={handleCreate}
                autoComplete="off"
                layout='vertical'
            >
                <Row gutter={16} className='w-full'>
                    <Col
                        xs={24}
                        md={12}
                        lg={12}
                    >
                        <Form.Item
                            label="Tên"
                            name="firstName"
                            rules={[{required: true, message: 'Hãy nhập tên của người dùng!'}]}
                        >
                            <Input
                                placeholder='Nhập tên người dùng'
                            />
                        </Form.Item>

                        <Form.Item
                            label="Họ"
                            name="lastName"
                            rules={[{required: true, message: 'Hãy nhập họ của người dùng!'}]}
                        >
                            <Input
                                placeholder='Nhập họ người dùng'
                            />
                        </Form.Item>
                        <Form.Item
                            label="Số điện thoại"
                            name="phone"
                            rules={[{required: true, message: 'Hãy nhập số điện thoại của người dùng!'}]}
                        >
                            <Input
                                placeholder='Nhập số điện thoại người dùng'
                            />
                        </Form.Item>
                    </Col>
                    <Col
                        xs={24}
                        md={12}
                        lg={12}
                    >
                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[{required: true, message: 'Hãy nhập email của người dùng!'}]}
                        >
                            <Input
                                placeholder='Nhập email người dùng'
                            />
                        </Form.Item>
                        <Form.Item
                            label="Mật khẩu"
                            name="pass"
                            rules={[{required: true, message: 'Hãy nhập mật khẩu của người dùng!'}]}
                        >
                            <Input
                                placeholder='Nhập mật khẩu người dùng'
                            />
                        </Form.Item>
                        <Form.Item
                            label="Trạng thái"
                            name="status"
                            rules={[{required: true, message: 'Hãy nhập trạng thái của người dùng!'}]}
                        >
                            <Input
                                placeholder='Nhập trạng thái người dùng'
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row className='justify-end'>
                    <Col className='mr-10'>
                        <Button onClick={() => setOpen(!open)}>
                            Hủy
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            type='primary'
                            htmlType='submit'
                            danger
                        >
                            Tạo mới
                        </Button>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )
        ;
};

