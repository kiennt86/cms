import { Button, Col, Form, Input, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../../app/types/api.ts'
import { REQUEST_ID } from '../../../../app/constants/appConstant.ts'
import { useEffect } from 'react'
import { UserInterface } from '../../../../app/types/user.ts'
import { useUpdateUserMutation } from '../../../../app/services/apiUserService.ts'


interface IProps {
  record: UserInterface
  onCancel: () => void
  fetchData: () => void
}

interface UserUpdate {
  firstname: string,
  lastname: string,
  phone: string,
  status: string,
  email: string,
  pass: string,
}

export const UserUpdate = ({ onCancel, fetchData, record }: IProps) => {
  const [userUpdate] = useUpdateUserMutation()
  const [form] = Form.useForm()

  useEffect(() => {
    if (record) {
      form.setFieldsValue({
        firstname: record.firstname,
        lastname: record.lastname,
        phone: record.phone,
        status: record.status,
        email: record.email,
        pass: record.pass
      })
    }
  }, [record])

  const handleUpdate = async (value: UserUpdate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.USER_UPDATE,
      requestId: REQUEST_ID,
      params: {
        firstname: value.firstname,
        lastname: value.lastname,
        phone: value.phone,
        status: value.status,
        email: value.email,
        pass: value.pass
      }
    }
    const result: any = await userUpdate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Cập nhật người dùng thành công' })
      onCancel()
      fetchData()
    } else {
      notification.error({ message: 'Cập nhật người nhật thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleUpdate}
      autoComplete='off'
      layout='vertical'
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Email'
            name='email'
          >
            <Input
              placeholder='Nhập email người dùng'
            />
          </Form.Item>
          <Form.Item
            label='Mật khẩu'
            name='pass'
          >
            <Input
              placeholder='Nhập mật khẩu người dùng'
            />
          </Form.Item>
          <Form.Item
            label='Tên người dùng'
            name='firstname'
            rules={[{ required: true, message: 'Hãy nhập tên của người dùng!' }]}
          >
            <Input
              placeholder='Nhập tên người dùng'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Họ người dùng'
            name='lastname'
            rules={[
              {
                required: true,
                message: 'Hãy nhập họ của người dùng!'
              }
            ]}>
            <Input
              placeholder='Nhập họ người dùng'
            />
          </Form.Item>
          <Form.Item
            label='Số điện thoại'
            name='phone'
            rules={[{ required: true, message: 'Hãy nhập số điện thoại của người dùng!' }]}
          >
            <Input
              placeholder='Nhập số điện thoại của người dùng'
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='status'
          >
            <Select
              options={[
                { value: 'ACTIVE', label: 'Hoạt động' },
                { value: 'INACTIVE', label: 'Không hoạt động' }
              ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Cập nhật
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

