import {MerchantUpdate} from "./MerchantUpdate.tsx";
import {MerchantCreate} from "./MerchantCreate.tsx";
import {MerchantDelete} from "./MerchantDelete.tsx";
import {Modal} from "antd";
import { MerchantInterface, viewMode } from '../../../../app/types/merchant.ts'
import { MerchantDetail } from './MerchantDetail.tsx'

interface IProps {
    mode: viewMode
    visible: boolean
    record: MerchantInterface
    onCancel: () => void
    fetchData: () => void
}
export const MerchantModal = ({
                                    onCancel,
                                    visible,
                                    fetchData,
                                    record,
                                    mode,
                                    ...props
                                }: IProps) => {

    const render = () => {
        switch (mode) {
            case viewMode.update:
                return (
                    <MerchantUpdate
                        onCancel={onCancel}
                        fetchData={fetchData}
                        record={record}
                    />
                )
            case viewMode.create:
                return (
                    <MerchantCreate
                        onCancel={onCancel}
                        fetchData={fetchData}
                    />
                )
            case viewMode.delete:
                return (
                    <MerchantDelete
                        record={record}
                        onCancel={onCancel}
                        fetchData={fetchData}
                    />
                )
          case viewMode.view:
            return (
              <MerchantDetail
                record={record}
                onCancel={onCancel}
              />
            )
        }
    }

    return (
        <Modal
            title={mode === viewMode.create ? "Tạo mới merchant" : mode === viewMode.update ? 'Cập nhật merchant' : mode === viewMode.view ? 'Xem chi tiết tài khoản nhúng':'Xoá merchant'}
            open={visible}
            footer={false}
            onCancel={onCancel}
            centered
            destroyOnClose={true}
            width={
                mode === viewMode.update ||
                mode === viewMode.create ||
                mode === viewMode.view
                    ? 800 : 600
            }
            maskClosable={false}
            mask
            {...props}
        >
            {render()}
        </Modal>
    )
}
