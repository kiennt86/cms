import {Button, notification, Typography} from 'antd'
import {useDeleteMerchantMutation} from "../../../../app/services/apiMerchantService.ts";
import {ApiCode, BodyInterface} from "../../../../app/types/api.ts";
import {REQUEST_ID} from "../../../../app/constants/appConstant.ts";
import { MerchantInterface } from '../../../../app/types/merchant.ts'

const {Text} = Typography

type DeleteConfirmModalType = {
    fetchData: () => void
    onCancel: () => void
    record: MerchantInterface
}

export const MerchantDelete = ({
                                   onCancel,
                                   record,
                                   fetchData
                               }: DeleteConfirmModalType) => {

    const [deleteMerchant] = useDeleteMerchantMutation()

    const onDelete = async () => {
        const body: BodyInterface = {
            apiCode: ApiCode.MERCHANT_DELETE,
            requestId: REQUEST_ID,
            params: {
                id: record.id,
            }
        }
        const result: any = await deleteMerchant(body)
        if (result?.data?.code === '00') {
            notification.success({message: 'Xóa merchant thành công'})
            onCancel()
            fetchData()
        } else {
            notification.error({message: 'Xóa merchant thất bại'})
        }
    }

    return (
        <>
            <Text style={{textAlign: 'center'}}>
                Bạn có chắc chắn muốn xoá merchant này không?
            </Text>
            <div style={{textAlign: 'end', marginTop: '8px'}}>
                <Button
                    className="mr-10"
                    onClick={() => {
                        onCancel()
                    }}
                >
                    Hủy
                </Button>
                <Button
                    type="primary"
                    onClick={onDelete}
                    danger
                >
                    Xóa
                </Button>
            </div>
        </>
    )
}
