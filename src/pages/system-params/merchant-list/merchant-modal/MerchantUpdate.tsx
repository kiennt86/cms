import { Button, Col, Form, Input, Modal, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../../app/types/api.ts'
import { REQUEST_ID } from '../../../../app/constants/appConstant.ts'
import { useUpdateMerchantMutation } from '../../../../app/services/apiMerchantService.ts'
import { useEffect } from 'react'
import { MerchantInterface } from '../../../../app/types/merchant.ts'


interface IProps {
  record: MerchantInterface
  onCancel: () => void
  fetchData: () => void
}

interface MerchantUpdate {
  code: string,
  name: string,
  agentCode: string,
  legalType: string,
  phone: string,
  active: number,
  bankAccount: string,
  taxcode: string,
}

export const MerchantUpdate = ({ onCancel, fetchData, record }: IProps) => {
  const [merchantUpdate, { isSuccess }] = useUpdateMerchantMutation()
  const [form] = Form.useForm()

  useEffect(() => {
    if (record) {
      form.setFieldsValue({
        code: record.code,
        name: record.name,
        agentCode: record.agentCode,
        legalType: record.legalType,
        active: record.active,
        phone: record.phone,
        bankAccount: record.bankAccount,
        taxcode: record.taxcode
      })
    }
  }, [record])

  const handleUpdate = async (value: MerchantUpdate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.MERCHANT_UPDATE,
      requestId: REQUEST_ID,
      params: {
        code: record.code,
        name: value.name,
        agentCode: value.agentCode,
        legalType: value.legalType,
        phone: value.phone,
        active: value.active,
        bankAccount: value.bankAccount,
        taxcode: value.taxcode
      }
    }
    const result: any = await merchantUpdate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Cập nhật merchant thành công' })
      onCancel()
      fetchData()
    } else {
      notification.error({ message: 'Cập nhật merchant thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleUpdate}
      autoComplete='off'
      layout='vertical'
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã số'
            name='code'
            rules={[{ required: true, message: 'Hãy nhập mã số của merchant!' }]}
          >
            <Input
              placeholder='Nhập mã số merchant'
            />
          </Form.Item>
          <Form.Item
            label='Tên'
            name='name'
            rules={[{ required: true, message: 'Hãy nhập tên của merchant!' }]}
          >
            <Input
              placeholder='Nhập tên merchant'
            />
          </Form.Item>

          <Form.Item
            label='Mã đại lý'
            name='agentCode'
            rules={[
              {
                required: true,
                message: 'Hãy nhập mã đại lý của merchant!'
              },
              {
                min: 1,
                max: 3,
                message: 'Mã đại lý không được vượt quá 3 ký tự!'
              }
            ]}>
            <Input
              placeholder='Nhập mã đại lý'
            />
          </Form.Item>
          <Form.Item
            label='Loại pháp lý'
            name='legalType'
            rules={[
              { required: true, message: 'Hãy nhập loại pháp lý của merchant!' },
              {
                min: 1,
                max: 2,
                message: 'Loại pháp lý không được vượt quá 2 ký tự!'
              }
            ]}>
            <Input
              placeholder='Nhập loại pháp lý'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Số điện thoại'
            name='phone'
            rules={[{ required: true, message: 'Hãy nhập số điện thoại của merchant!' }]}
          >
            <Input
              placeholder='Nhập số điện thoại'
            />
          </Form.Item>
          <Form.Item
            label='Tài khoản ngân hàng'
            name='bankAccount'
            rules={[{ required: true, message: 'Hãy nhập tài khoản ngân hàng của merchant!' }]}
          >
            <Input
              placeholder='Nhập tài khoản ngân hàng'
            />
          </Form.Item>
          <Form.Item
            label='Mã số thuế'
            name='taxcode'
            rules={[
              {
                required: true,
                message: 'Hãy nhập mã số thuế của merchant!'
              }
            ]}
          >
            <Input
              placeholder='Nhập mã số thuế'
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='active'
          >
            <Select
              options={[
                { value: 1, label: 'Hoạt động' },
                { value: 0, label: 'Không hoạt động' }
              ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Cập nhật
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

