import { Button, Col, Form, Input, Modal, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../../app/types/api.ts'
import { REQUEST_ID } from '../../../../app/constants/appConstant.ts'
import { useCreateMerchantMutation } from '../../../../app/services/apiMerchantService.ts'
import dayjs from 'dayjs'


interface IProps {
  onCancel: () => void
  fetchData: () => void
}

interface MerchantCreate {
  code: string,
  name: string,
  agentCode: string,
  legalType: number,
  phone: string,
  active: number,
  bankAccount: string,
  taxcode: string,
  registrationDate: string
}

export const MerchantCreate = ({ onCancel, fetchData }: IProps) => {
  const [merchantCreate, { isSuccess }] = useCreateMerchantMutation()
  const [form] = Form.useForm()
  const handleCreate = async (value: MerchantCreate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.MERCHANT_CREATE,
      requestId: REQUEST_ID,
      params: {
        code: value.code,
        name: value.name,
        agentCode: value.agentCode,
        legalType: value.legalType,
        phone: value.phone,
        active: value.active,
        bankAccount: value.bankAccount,
        taxcode: value.taxcode,
        registrationDate: new Date(dayjs().toISOString()).getTime()
      }
    }
    const result: any = await merchantCreate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Tạo mới merchant thành công' })
      onCancel()
      form.resetFields()
      fetchData()
    } else if (result?.data?.msgRepose == 'merchant exits.') {
      notification.error({ message: 'Merchant đã tồn tại' })
    } else {
      notification.error({ message: 'Tạo mới merchant thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleCreate}
      autoComplete='off'
      layout='vertical'
      initialValues={{
        active: 1
      }}
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã số'
            name='code'
            rules={[{ required: true, message: 'Hãy nhập mã số của merchant!' }]}
          >
            <Input
              placeholder='Nhập mã số merchant'
            />
          </Form.Item>
          <Form.Item
            label='Tên'
            name='name'
            rules={[{ required: true, message: 'Hãy nhập tên của merchant!' }]}
          >
            <Input
              placeholder='Nhập tên merchant'
            />
          </Form.Item>

          <Form.Item
            label='Mã đại lý'
            name='agentCode'
            rules={[
              {
                required: true,
                message: 'Hãy nhập mã đại lý của merchant!'
              },
              {
                min: 1,
                max: 3,
                message: 'Mã đại lý không được vượt quá 3 ký tự!'
              }
            ]}>
            <Input
              placeholder='Nhập mã đại lý'
            />
          </Form.Item>
          <Form.Item
            label='Loại pháp lý'
            name='legalType'
            rules={[
              { required: true, message: 'Hãy nhập loại pháp lý của merchant!' },
              {
                min: 1,
                max: 2,
                message: 'Loại pháp lý không được vượt quá 2 ký tự!'
              }
            ]}
          >
            <Input
              type='number'
              placeholder='Nhập loại pháp lý'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Số điện thoại'
            name='phone'
            rules={[{ required: true, message: 'Hãy nhập số điện thoại của merchant!' }]}
          >
            <Input
              placeholder='Nhập số điện thoại'
            />
          </Form.Item>
          <Form.Item
            label='Tài khoản ngân hàng'
            name='bankAccount'
            rules={[{ required: true, message: 'Hãy nhập tài khoản ngân hàng của merchant!' }]}
          >
            <Input
              placeholder='Nhập tài khoản ngân hàng'
            />
          </Form.Item>
          <Form.Item
            label='Mã số thuế'
            name='taxcode'
            rules={[
              {
                required: true,
                message: 'Hãy nhập mã số thuế của merchant!'
              }
            ]}
          >
            <Input
              placeholder='Nhập mã số thuế'
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='active'
          >
            <Select
              options={[
                { value: 1, label: 'Hoạt động' },
                { value: 0, label: 'Không hoạt động' }
              ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Tạo mới
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

