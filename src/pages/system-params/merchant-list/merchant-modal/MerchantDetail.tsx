import { Button, Col, Divider, Form, Row, Table, Tag, Tooltip, Typography } from 'antd'
import { MerchantDipAccountDetail, MerchantInterface, viewMode } from '../../../../app/types/merchant.ts'
import { ColumnsType } from 'antd/es/table'

const { Text } = Typography

type IType = {
  record: MerchantInterface
  onCancel: () => void
}

export const MerchantDetail = ({
                                 onCancel,
                                 record
                               }: IType) => {


  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  const columns: ColumnsType<MerchantDipAccountDetail> = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'UUID',
      dataIndex: 'uuid',
      key: 'uuid',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên tài khoản',
      dataIndex: 'accountName',
      key: 'accountName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }

    },
    {
      title: 'ID người dùng',
      dataIndex: 'userId',
      key: 'userId',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    }
  ]

  return (
    <>
      <Table columns={columns}
             dataSource={record?.dipAccount}
             scroll={{ x: '100%' }}
             rowKey={''}
             pagination={{
               showSizeChanger: false,
               defaultPageSize: 10,
             }}
      />
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
      </Row>
    </>

  )
}
