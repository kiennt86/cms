import {
  PlusOutlined,
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
  EyeOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col, DatePicker,
  Divider,
  Form,
  Input,
  Row,
  Table, Tag, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { getStatusText, getTagColor } from '../../../app/constants/helperFunctions.ts'
import { useEffect, useMemo, useState } from 'react'
import { ApiCode } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useGetListMerchantsMutation } from '../../../app/services/apiMerchantService.ts'
import dayjs from 'dayjs'
import { MerchantModal } from './merchant-modal/MerchantModal.tsx'
import { MerchantInterface, MerchantSearchParams, viewMode } from '../../../app/types/merchant.ts'

export const MerchantList = () => {
  const { Text } = Typography
  const { RangePicker } = DatePicker
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'
  const [getMerchantListMutation] = useGetListMerchantsMutation()
  const [merchantData, setMerchantData] = useState<MerchantInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<MerchantInterface>()
  const [form] = Form.useForm()
  const [search, setSearch] = useState<MerchantSearchParams>({})
  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  async function fetchData() {
    try {
      const body = {
        apiCode: ApiCode.MERCHANT_LIST,
        requestId: REQUEST_ID,
        params: search
      }
      const merchants: any = await getMerchantListMutation(body)
      setMerchantData(merchants?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [search])

  const merchantList = useMemo(() => {
    if (merchantData && merchantData?.length > 0) {
      return merchantData?.map((item: MerchantInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [merchantData])

  const handleModal = (action: viewMode, record?: MerchantInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }

  const handleSearch = (values: MerchantSearchParams) => {
    const query = {
      ...values,
      taxcode: values.taxcode === '' ? undefined : values.taxcode?.trim(),
      name: values.name === '' ? undefined : values.name?.trim(),
      agentCode: values.agentCode === '' ? undefined : values.agentCode?.trim(),
      fromDate: values.time === undefined || values.time === null ? undefined : new Date(dayjs(values.time[0]).startOf('day').toISOString()).getTime(),
      toDate: values.time === undefined || values.time === null ? undefined : new Date(dayjs(values.time[1]).endOf('day').toISOString()).getTime()
    }
    const newQuery = {}
    for (const key in query) {
      if (query[key] && query[key] !== undefined && query[key] !== null) {
        newQuery[key] = query[key]
      }
    }
    setSearch(newQuery)

  }

  const columns: ColumnsType<MerchantInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã số thuế',
      dataIndex: 'taxcode',
      key: 'taxcode',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }

    },
    {
      title: 'Mã đại lý',
      dataIndex: 'agentCode',
      key: 'agentCode',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Loại pháp lý',
      key: 'legalType',
      dataIndex: 'legalType',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã số',
      key: 'code',
      dataIndex: 'code',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số điện thoại',
      key: 'phone',
      dataIndex: 'phone',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số tài khoản ngân hàng của ví',
      key: 'bankAccount',
      dataIndex: 'bankAccount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Ngày đăng ký',
      key: 'registrationDate',
      dataIndex: 'registrationDate',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'Trạng thái liên kết',
      key: 'active',
      dataIndex: 'active',
      align: 'center',
      render: (record: boolean | string | number) => (
        <Tag
          color={getTagColor(record)}
          className='text-capitalize'
        >
          {getStatusText(record)}
        </Tag>
      )
    },
    {
      title: 'Tác vụ',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (value, record) => (
        <Row
          gutter={5}
          justify='center'
        >
          <Col>
            <Tooltip
              placement='top'
              title='Xem'
            >
              <Button
                icon={<EyeOutlined />}
                onClick={() => {
                  handleModal(viewMode.view, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
          <Col>
            <Tooltip
              placement='top'
              title='Sửa'
            >
              <Button
                icon={<EditOutlined />}
                onClick={() => {
                  handleModal(viewMode.update, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
          <Col>
            <Tooltip
              placement='top'
              title='Xóa'
            >
              <Button
                type='primary'
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  handleModal(viewMode.delete, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
        </Row>
      )
    }
  ]

  return (
    <Card title='Danh sách merchant'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
          className='header'
        >
          <Form
            className='w-100'
            layout='vertical'
            onFinish={handleSearch}
            form={form}
            autoComplete='off'
          >
            <Row gutter={16}>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='agentCode'
                  label='Mã đại lý'
                >
                  <Input
                    allowClear
                    placeholder='Nhập mã đại lý'
                    onChange={(e) => {
                      if (e.target.value === '' || e.target.value === undefined) {
                        setSearch({
                          ...search,
                          agentCode: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='name'
                  label='Tên merchant'
                >
                  <Input
                    allowClear
                    placeholder='Hãy nhập tên merchant'
                    onChange={(e) => {
                      if (e.target.value === '' || e.target.value === undefined) {
                        setSearch({
                          ...search,
                          name: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='time'
                  label='Ngày tạo'
                >
                  <RangePicker
                    format={dateFormat}
                    allowClear
                    onChange={(e) => {
                      if (e === null) {
                        setSearch({
                          ...search,
                          fromDate: undefined,
                          toDate: undefined
                        })
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <Col className='mt-30'>
                <Button
                  danger
                  type='primary'
                  htmlType='submit'
                  icon={<SearchOutlined />}
                >
                  Truy vấn
                </Button>
              </Col>
              <Col className='mt-30'>
                <Button
                  icon={<PlusOutlined />}
                  onClick={() => {
                    handleModal(viewMode.create)
                  }}
                >
                  Thêm mới
                </Button>
              </Col>
            </Row>
          </Form>
          <Divider />
        </Col>
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={merchantList}
                 scroll={{ x: '100%' }}
                 rowKey='no'
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
      <MerchantModal
        mode={mode}
        visible={visible}
        onCancel={onCancel}
        record={selectedRecord}
        fetchData={fetchData}
      />
    </Card>
  )
}

