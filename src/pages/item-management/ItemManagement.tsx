import {
  PlusOutlined,
  DeleteOutlined, EyeOutlined, EditOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col,
  Divider,
  Row,
  Table, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useMemo, useState } from 'react'
import { ApiCode, viewMode } from '../../app/types/api.ts'
import { REQUEST_ID } from '../../app/constants/appConstant.ts'
import { ItemModal } from './item-modal/ItemModal.tsx'
import { useGetListItemsMutation } from '../../app/services/apiItemService.ts'
import { ItemInterface } from '../../app/types/item.ts'

export const ItemManagement = () => {
  const { Text } = Typography
  const [getListItemsMutation] = useGetListItemsMutation()
  const [itemData, setItemData] = useState<ItemInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<ItemInterface>()

  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  const fetchData = async () => {
    try {
      const body = {
        apiCode: ApiCode.ITEM_GET,
        requestId: REQUEST_ID,
        params: {}
      }

      const items: any = await getListItemsMutation(body)
      setItemData(items?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  const itemList = useMemo(() => {
    if (itemData && itemData?.length > 0) {
      return itemData?.map((item: ItemInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [itemData])


  const handleModal = (action: viewMode, record?: ItemInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }


  const columns: ColumnsType<ItemInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã Item',
      dataIndex: 'itemCode',
      key: 'itemCode',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên Item',
      dataIndex: 'itemName',
      key: 'itemName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số tài khoản người sở hữu Item',
      dataIndex: 'itemAccountNumber',
      key: 'itemAccountNumber',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên tài khoản người sở hữu Item',
      dataIndex: 'itemOwnerAccountName',
      key: 'itemOwnerAccountName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Nhà cung cấp',
      dataIndex: 'itemSupplier',
      key: 'itemSupplier',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Danh mục',
      key: 'itemCategory',
      dataIndex: 'itemCategory',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên danh mục',
      key: 'itemCategoryName',
      dataIndex: 'itemCategoryName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Trạng thái',
      key: 'itemStatus',
      dataIndex: 'itemStatus',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tác vụ',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (value, record) => (
        <Row
          gutter={5}
          justify='center'
        >
          <Col>
            <Tooltip
              placement='top'
              title='Xem'
            >
              <Button
                icon={<EyeOutlined />}
                onClick={() => {
                  handleModal(viewMode.view, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
          <Col>
            <Tooltip
              placement='top'
              title='Sửa'
            >
              <Button
                icon={<EditOutlined />}
                onClick={() => {
                  handleModal(viewMode.update, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
          <Col>
            <Tooltip
              placement='top'
              title='Xóa'
            >
              <Button
                type='primary'
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  handleModal(viewMode.delete, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
        </Row>
      )
    }
  ]

  return (
    <Card title='Quản lý mặt hàng'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col>
          <Button
            className='mr-10'
            type='primary'
            danger
            icon={<PlusOutlined />}
            onClick={() => {
              handleModal(viewMode.create)
            }}
          >
            Thêm mới
          </Button>
        </Col>
        <Divider />
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={itemList}
                 scroll={{ x: '100%' }}
                 rowKey={'no'}
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
      <ItemModal
        mode={mode}
        visible={visible}
        onCancel={onCancel}
        record={selectedRecord}
        fetchData={fetchData}
      />
    </Card>
  )
}

