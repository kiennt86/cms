import { Button, notification, Typography } from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { ItemInterface } from '../../../app/types/item.ts'
import { useDeleteItemMutation } from '../../../app/services/apiItemService.ts'

const { Text } = Typography

type DeleteConfirmModalType = {
  fetchData: () => void
  onCancel: () => void
  record: ItemInterface
}

export const ItemDelete = ({
                             onCancel,
                             record,
                             fetchData
                           }: DeleteConfirmModalType) => {

  const [deleteItem] = useDeleteItemMutation()

  const onDelete = async () => {
    const body: BodyInterface = {
      apiCode: ApiCode.ITEM_DELETE,
      requestId: REQUEST_ID,
      params: {
        itemCode: record.itemCode
      }
    }
    const result: any = await deleteItem(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Xóa Item thành công' })
      onCancel()
      fetchData()
    } else {
      notification.error({ message: 'Xóa Item thất bại' })
    }
  }

  return (
    <>
      <Text style={{ textAlign: 'center' }}>
        Bạn có chắc chắn muốn xoá Item này không?
      </Text>
      <div style={{ textAlign: 'end', marginTop: '8px' }}>
        <Button
          className='mr-10'
          onClick={() => {
            onCancel()
          }}
        >
          Hủy
        </Button>
        <Button
          type='primary'
          onClick={onDelete}
          danger
        >
          Xóa
        </Button>
      </div>
    </>
  )
}
