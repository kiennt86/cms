import { Button, Col, Form, Input, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useUpdateItemMutation } from '../../../app/services/apiItemService.ts'
import { ItemInterface } from '../../../app/types/item.ts'
import { useEffect } from 'react'


interface IProps {
  onCancel: () => void
  fetchData: () => void
  record: ItemInterface
}

interface ItemUpdate {
  itemId: string,
  status: string

}

export const ItemUpdate = ({ onCancel, fetchData, record }: IProps) => {
  const [itemUpdate, { isSuccess }] = useUpdateItemMutation()
  const [form] = Form.useForm()

  useEffect(() => {
    if (record) {
      form.setFieldsValue({
        itemId: record.itemId,
        status: record.itemStatus
      })
    }
  }, [record])

  const handleUpdate = async (value: ItemUpdate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.ITEM_UPDATE,
      requestId: REQUEST_ID,
      params: {
        itemId: value.itemId,
        status: value.status
      }
    }
    const result: any = await itemUpdate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Cập nhật thành công' })
      onCancel()
      fetchData()
    } else {
      notification.error({ message: 'Cập nhật thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleUpdate}
      autoComplete='off'
      layout='vertical'
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='ID'
            name='itemId'
          >
            <Input
              placeholder='Hãy nhập ID mặt hàng'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Trạng thái'
            name='status'
          >
            <Select
              placeholder='Chọn trạng thái'
              options={
                [{ value: 'ISSUE', label: 'ISSUE' },
                  { value: 'TRADE', label: 'TRADE' },
                  { value: 'DELETE', label: 'DELETE' }
                ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Cập nhật
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

