import { Divider, Modal } from 'antd'
import { viewMode } from '../../../app/types/api.ts'
import { ItemUpdate } from './ItemUpdate.tsx'
import { ItemDelete } from './ItemDelete.tsx'
import { ItemCreate } from './ItemCreate.tsx'
import { ItemInterface } from '../../../app/types/item.ts'
import { ItemDetail } from './ItemDetail.tsx'


interface IProps {
  mode: viewMode
  visible: boolean
  record: ItemInterface
  onCancel: () => void
  fetchData: () => void
}

export const ItemModal = ({
                            onCancel,
                            visible,
                            fetchData,
                            record,
                            mode,
                            ...props
                          }: IProps) => {

  const render = () => {
    switch (mode) {
      case viewMode.create:
        return (
          <ItemCreate
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
      case viewMode.delete:
        return (
          <ItemDelete
            record={record}
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
      case viewMode.update:
        return (
          <ItemUpdate
            onCancel={onCancel}
            fetchData={fetchData}
            record={record}
          />
        )
      case viewMode.view:
        return (
          <ItemDetail
            onCancel={onCancel}
            record={record}
          />
        )
    }
  }

  return (
    <Modal
      title={mode === viewMode.create ? 'Tạo mới Item' : mode === viewMode.update ? 'Cập nhật Item' : mode === viewMode.view ? 'Xem chi tiết' : 'Xoá Item'}
      open={visible}
      footer={false}
      onCancel={onCancel}
      centered
      destroyOnClose={true}
      width={
        mode === viewMode.update ||
        mode === viewMode.create ||
        mode === viewMode.view
          ? 800 : 600
      }
      maskClosable={false}
      mask
      {...props}
    >
      {render()}
    </Modal>
  )
}
