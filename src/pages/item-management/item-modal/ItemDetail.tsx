import { Button, Col, Form, Input, Row } from 'antd'
import { useEffect } from 'react'
import { ItemInterface } from '../../../app/types/item.ts'
import dayjs from 'dayjs'

type IType = {
  record: ItemInterface
  onCancel: () => void
}

export const ItemDetail = ({
                             onCancel,
                             record
                           }: IType) => {

  const [form] = Form.useForm()
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'

  useEffect(() => {
    if (record) {
      form.setFieldsValue({
        itemCode: record.itemCode ? record.itemCode : '-',
        itemName: record.itemName ? record.itemName : '-',
        itemAccountNumber: record.itemAccountNumber ? record.itemAccountNumber : '-',
        itemOwnerAccountName: record.itemOwnerAccountName ? record.itemOwnerAccountName : '-',
        itemSupplier: record.itemSupplier ? record.itemSupplier : '-',
        itemCategory: record.itemCategory ? record.itemCategory : '-',
        itemCategoryName: record.itemCategoryName ? record.itemCategoryName : '-',
        itemValue: record.itemValue ? record.itemValue : '-',
        itemStatus: record.itemStatus ? record.itemStatus : '-',
        itemInterestRate: record.itemInterestRate ? record.itemInterestRate : '-',
        itemTier: record.itemTier ? record.itemTier : '-',
        itemTxnDate: record.itemTxnDate ? dayjs(record.itemTxnDate).format(dateFormat) : '-',
        itemIssuer: record.itemIssuer ? record.itemIssuer : '-'
      })
    }
  }, [record])

  return (
    <Form
      layout='vertical'
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã Item'
            name='itemCode'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tên Item'
            name='itemName'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Số tài khoản người sở hữu Item'
            name='itemAccountNumber'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tên tài khoản người sở hữu Item'
            name='itemOwnerAccountName'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Nhà cung cấp'
            name='itemSupplier'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Danh mục'
            name='itemCategory'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tên danh mục'
            name='itemCategoryName'
          >
            <Input
              readOnly
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Giá trị'
            name='itemValue'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tỷ lệ lãi suất'
            name='itemInterestRate'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Phương thức tính lãi suất'
            name='itemTier'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Nhà phát hành'
            name='itemIssuer'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Txn Date'
            name='itemTxnDate'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='itemStatus'
          >
            <Input
              readOnly
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
      </Row>
    </Form>
  )
}
