import { Button, Col, Form, Input, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useCreateItemMutation } from '../../../app/services/apiItemService.ts'


interface IProps {
  onCancel: () => void
  fetchData: () => void
}

interface IItemCreate {
  itemOwnerAccountName: string,
  itemName: string,
  itemAccountNumber: string,
  itemIssuer: string,
  itemSupplier: string
  itemCategory: string
  itemTier: string
  itemInterestRate: number
}

export const ItemCreate = ({ onCancel, fetchData }: IProps) => {
  const [itemCreate] = useCreateItemMutation()
  const [form] = Form.useForm()
  const handleCreate = async (value: IItemCreate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.ITEM_CREATE,
      requestId: REQUEST_ID,
      params: {
        itemOwnerAccountName: value.itemOwnerAccountName,
        itemName: value.itemName,
        itemAccountNumber: value.itemAccountNumber,
        itemSupplier: value.itemSupplier,
        itemCategory: value.itemCategory,
        itemTier: value.itemTier,
        itemInterestRate: value.itemInterestRate
      }
    }
    const result: any = await itemCreate(body)
    if (result?.data?.code === '00') {
      notification.success({ message: 'Tạo mới sản phẩm thành công' })
      onCancel()
      form.resetFields()
      fetchData()
    } else {
      notification.error({ message: 'Tạo mới sản phẩm thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleCreate}
      autoComplete='off'
      layout='vertical'
      form={form}
    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Tên tài khoản người sở hữu sản phẩm'
            name='itemOwnerAccountName'
            rules={[{ required: true, message: 'Hãy nhập tên tài khoản người sở hữu sản phẩm!' }]}
          >
            <Input
              placeholder='Nhập tên tài khoản người sở hữu sản phẩm'
            />
          </Form.Item>
          <Form.Item
            label='Tên sản phẩm'
            name='itemName'
            rules={[{ required: true, message: 'Hãy nhập tên sản phẩm!' }]}
          >
            <Input
              placeholder='Nhập tên sản phẩm'
            />
          </Form.Item>

          <Form.Item
            label='Số tài khoản người sở hữu sản phẩm'
            name='itemAccountNumber'
            rules={[
              {
                required: true,
                message: 'Hãy nhập số tài khoản người sở hữu sản phẩm!'
              }
            ]}>
            <Input
              placeholder='Nhập số tài khoản người sở hữu sản phẩm'
            />
          </Form.Item>
          <Form.Item
            label='Nhà phát hành'
            name='itemIssuer'
            rules={[{ required: true, message: 'Hãy nhập nhà phát hành!' }]}
          >
            <Input
              placeholder='Nhập nhà phát hành'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Nhà cung cấp'
            name='itemSupplier'
            rules={[{ required: true, message: 'Hãy nhập nhà cung cấp!' }]}
          >
            <Input
              placeholder='Nhập nhà cung cấp'
            />
          </Form.Item>
          <Form.Item
            label='Danh mục sản phẩm'
            name='itemCategory'
            rules={[{ required: true, message: 'Hãy nhập danh mục sản phẩm!' }]}
          >
            <Input
              placeholder='Nhập danh mục sản phẩm'
            />
          </Form.Item>
          <Form.Item
            label='Phương thức tính lãi suất'
            name='itemTier'
            rules={[{ required: true, message: 'Hãy nhập phương thức tính lãi suất!' }]}
          >
            <Select
              placeholder='Chọn phương thức tính lãi suất'
              options={
                [{ value: 'SINGLE', label: 'SINGLE' },
                  { value: 'BAND', label: 'BAND' },
                  { value: 'LEVEL', label: 'LEVEL' }
                ]}
            />
          </Form.Item>
          <Form.Item
            label='Tỷ lệ lãi suất'
            name='itemInterestRate'
            rules={[{ required: true, message: 'Hãy nhập tỷ lệ lãi suất!' }]}
          >
            <Input
              placeholder='Nhập tỷ lệ lãi suất'
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Tạo mới
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

