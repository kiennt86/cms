import {
    ReloadOutlined,
    SearchOutlined,
    FileExcelOutlined
} from '@ant-design/icons'
import {
    Button, Card,
    Col, DatePicker,
    Divider,
    Form,
    Input,
    Row,
    Table, Tag,
} from 'antd'
import {ColumnsType} from "antd/es/table";
import {getStatusText, getTagColor} from "../../app/constants/helperFunctions.ts";

interface DataType {
    key: string;
    type: string;
    transactionCode: string;
    customerCode: string;
    merchantAccount: string;
    merchantName: string;
    merchantBankName: string;
    moneyType: string;
    totalAmount: number;
    transactionContent: string;
    transactionTime: string;
    transactionStatus: number;
}

export const MerchantTransactionManagement = () => {
    const {RangePicker} = DatePicker;
    const dateFormat = 'YYYY/MM/DD';

    const columns: ColumnsType<DataType> = [
        {
            title: 'STT',
            dataIndex: 'key',
            key: 'key',
            align: 'center',
        },
        {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
            align: 'center',
        },
        {
            title: 'Mã giao dịch',
            dataIndex: 'transactionCode',
            key: 'transactionCode',
            align: 'center',
        },
        {
            title: 'Mã KH giao dịch',
            dataIndex: 'customerCode',
            key: 'customerCode',
            align: 'center',
        },
        {
            title: 'Số tài khoản merchant',
            key: 'merchantAccount',
            dataIndex: 'merchantAccount',
            align: 'center',
        },
        {
            title: 'Tên merchant',
            key: 'merchantName',
            dataIndex: 'merchantName',
            align: 'center',
        },
        {
            title: 'Tên ngân hàng merchant',
            key: 'merchantBankName',
            dataIndex: 'merchantBankName',
            align: 'center',
        },
        {
            title: 'Loại tiền',
            key: 'moneyType',
            dataIndex: 'moneyType',
            align: 'center',
        },
        {
            title: 'Tổng số tiền',
            key: 'totalAmount',
            dataIndex: 'totalAmount',
            align: 'center',
        },
        {
            title: 'Nội dung giao dịch',
            key: 'transactionContent',
            dataIndex: 'transactionContent',
            align: 'center',
        },
        {
            title: 'Thời gian giao dịch',
            key: 'transactionTime',
            dataIndex: 'transactionTime',
            align: 'center',
        },
        {
            title: 'Trạng thái giao dịch',
            key: 'transactionStatus',
            dataIndex: 'transactionStatus',
            align: 'center',
            render: (record: boolean) => (
                <Tag
                    color={getTagColor(record)}
                    className='text-capitalize'
                >
                    {getStatusText(record)}
                </Tag>
            )
        }
    ];

    const data: DataType[] = [
        {
            key: '1',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '2',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 0
        },
        {
            key: '3',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '4',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '5',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '6',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '7',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '8',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '9',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '10',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '11',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
        {
            key: '12',
            type: 'IN',
            transactionCode: 'FT00001',
            customerCode: '780000',
            merchantAccount: '88888888',
            merchantName: 'Momo',
            merchantBankName: 'TPBank',
            moneyType: 'VNĐ',
            totalAmount: 1000000,
            transactionContent: 'FT00001_Chuyển tiền vào tk phát lộc',
            transactionTime: '00:00:00 24/1/2024',
            transactionStatus: 1
        },
    ];


    return (
        <Card title='Quản lý TTGD merchant'>
            <Row
                gutter={[0, 16]}
                className='border-rounded'
            >
                <Col
                    span={24}
                    className='header'
                >
                    <Form
                        className='w-100'
                        layout='vertical'
                    >
                        <Row gutter={16}>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='merchant'
                                    label='Merchant'
                                >
                                    <Input
                                        placeholder='Nhập merchant'
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='transactionCode'
                                    label='Mã giao dịch'
                                >
                                    <Input
                                        placeholder='Nhập mã giao dịch'
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='createdAt'
                                    label='Ngày tạo'
                                >
                                    <RangePicker
                                        className='w-100'
                                        format={dateFormat}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row
                            justify='end'
                            gutter={16}
                        >
                            <Col>
                                <Button
                                    danger
                                    type='primary'
                                    htmlType='submit'
                                    icon={<SearchOutlined/>}
                                >
                                    Truy vấn
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    icon={<ReloadOutlined/>}
                                >
                                    Thiết lập lại
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    icon={<FileExcelOutlined/>}
                                >
                                    Xuất file
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                    <Divider/>
                </Col>
                <Col
                    span={24}
                    className='content'
                >
                    <Table columns={columns}
                           dataSource={data}
                           scroll={{x: '100%'}}
                           rowKey={'key'}
                           pagination={{
                               pageSizeOptions: ['10', '20', '50', '100'],
                               showSizeChanger: true,
                               defaultPageSize: 10,
                               showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`, // Hiển thị tổng số record
                           }}
                    />
                </Col>
            </Row>
        </Card>
    )
}

