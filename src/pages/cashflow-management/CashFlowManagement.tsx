import {
    FileExcelOutlined,
    ReloadOutlined,
    SearchOutlined
} from '@ant-design/icons'
import {
    Button, Card,
    Col, DatePicker,
    Divider,
    Form,
    Input,
    Row,
    Table
} from 'antd'
import {ColumnsType} from "antd/es/table";

interface DataType {
    key: string;
    customerCode: string;
    merchantName: string;
    merchantCode: string;
    moneyType: string;
    depositTotal: string;
    withdrawTotal: string;
    realInterest: string;
    depositTimes: number;
    withdrawTimes: number;
}

export const CashFlowManagement = () => {
    const {RangePicker} = DatePicker;
    const dateFormat = 'YYYY/MM/DD';

    const columns: ColumnsType<DataType> = [
        {
            title: 'STT',
            dataIndex: 'key',
            key: 'key',
            align: 'center',
        },
        {
            title: 'Mã KH giao dịch',
            dataIndex: 'customerCode',
            key: 'customerCode',
            align: 'center',
        },
        {
            title: 'Tên merchant',
            dataIndex: 'merchantName',
            key: 'merchantName',
            align: 'center',
        },
        {
            title: 'Mã merchant',
            dataIndex: 'merchantCode',
            key: 'merchantCode',
            align: 'center',
        },
        {
            title: 'Loại tiền',
            key: 'moneyType',
            dataIndex: 'moneyType',
            align: 'center',
        },
        {
            title: 'Tổng số tiền gửi vào',
            key: 'depositTotal',
            dataIndex: 'depositTotal',
            align: 'center',
        },
        {
            title: 'Tổng số tiền rút ra',
            key: 'withdrawTotal',
            dataIndex: 'withdrawTotal',
            align: 'center',
        },
        {
            title: 'Tiền lãi thực trả KH phát sinh',
            key: 'realInterest',
            dataIndex: 'realInterest',
            align: 'center',
        },
        {
            title: 'Số lần gửi',
            key: 'depositTimes',
            dataIndex: 'depositTimes',
            align: 'center',
        },
        {
            title: 'Số lần rút',
            key: 'withdrawTimes',
            dataIndex: 'withdrawTimes',
            align: 'center',

        },
    ]

    const data: DataType[] = [
        {
            key: '1',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '2',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '3',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '4',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '5',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '6',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '7',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '8',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '9',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '10',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '11',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
        {
            key: '12',
            customerCode: '0123456789',
            merchantName: 'Momo',
            merchantCode: 'M01',
            moneyType: 'VNĐ',
            depositTotal: '1000000',
            withdrawTotal: '50000',
            realInterest: '20000',
            depositTimes: 6,
            withdrawTimes: 5
        },
    ];


    return (
        <Card title='Quản lý dòng tiền'>
            <Row
                gutter={[0, 16]}
                className='border-rounded'
            >
                <Col
                    span={24}
                    className='header'
                >
                    <Form
                        className='w-100'
                        layout='vertical'
                    >
                        <Row gutter={16}>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='customerCode'
                                    label='Mã khách hàng'
                                >
                                    <Input
                                        placeholder='Nhập mã khách hàng'
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='transactionCode'
                                    label='Mã giao dịch'
                                >
                                    <Input
                                        placeholder='Nhập mã giao dịch'
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='createdAt'
                                    label='Ngày tạo'
                                >
                                    <RangePicker
                                        className='w-100'
                                        format={dateFormat}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row
                            justify='end'
                            gutter={16}
                        >
                            <Col>
                                <Button
                                    danger
                                    type='primary'
                                    htmlType='submit'
                                    icon={<SearchOutlined/>}
                                >
                                    Truy vấn
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    icon={<ReloadOutlined/>}
                                >
                                    Thiết lập lại
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    icon={<FileExcelOutlined/>}
                                >
                                    Xuất file
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                    <Divider/>
                </Col>
                <Col
                    span={24}
                    className='content'
                >
                    <Table columns={columns}
                           dataSource={data}
                           scroll={{x: '100%'}}
                           rowKey={'key'}
                           pagination={{
                               pageSizeOptions: ['10', '20', '50', '100'],
                               showSizeChanger: true,
                               defaultPageSize: 10,
                               showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`, // Hiển thị tổng số record
                           }}
                    />
                </Col>
            </Row>
        </Card>
    )
}

