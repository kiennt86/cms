import {
    ReloadOutlined,
    SearchOutlined,
    FileExcelOutlined
} from '@ant-design/icons'
import {
    Button, Card,
    Col, DatePicker,
    Divider,
    Form,
    Input,
    Row,
    Table,
} from 'antd'
import {ColumnsType} from "antd/es/table";

interface DataType {
    key: string;
    accountName: string;
    accountNumber: string;
    bank: string;
    fundManagement: string;
    openingBalance: number;
    currentBalance: number;
    investmentProduct: string;
    startTime: string;
    endingTime: string;
    profitRate: string;
    profit: number;
}

export const InvestmentManagement = () => {
    const {RangePicker} = DatePicker;
    const dateFormat = 'YYYY/MM/DD';

    const columns: ColumnsType<DataType> = [
        {
            title: 'STT',
            dataIndex: 'key',
            key: 'key',
            align: 'center',
        },
        {
            title: 'Tên chủ tài khoản',
            dataIndex: 'accountName',
            key: 'accountName',
            align: 'center',
        },
        {
            title: 'Số tài khoản',
            dataIndex: 'accountNumber',
            key: 'accountNumber',
            align: 'center',
        },
        {
            title: 'Ngân hàng lưu ký',
            dataIndex: 'bank',
            key: 'bank',
            align: 'center',
        },
        {
            title: 'Quỹ quản lý',
            key: 'fundManagement',
            dataIndex: 'fundManagement',
            align: 'center',
        },
        {
            title: 'Số dư đầu tư đầu kỳ',
            key: 'openingBalance',
            dataIndex: 'openingBalance',
            align: 'center',
        },
        {
            title: 'Số dư hiện tại',
            key: 'currentBalance',
            dataIndex: 'currentBalance',
            align: 'center',
        },
        {
            title: 'Sản phẩm đầu tư',
            key: 'investmentProduct',
            dataIndex: 'investmentProduct',
            align: 'center',
        },
        {
            title: 'Ngày bắt đầu chu kỳ đầu tư',
            key: 'startTime',
            dataIndex: 'startTime',
            align: 'center',
        },
        {
            title: 'Ngày kết thúc chu kỳ đầu tư',
            key: 'endingTime',
            dataIndex: 'endingTime',
            align: 'center',
        },
        {
            title: 'Tỷ suất sinh lời kỳ vọng (%/Năm)',
            key: 'profitRate',
            dataIndex: 'profitRate',
            align: 'center',
        },
        {
            title: 'Lợi nhuận',
            key: 'profit',
            dataIndex: 'profit',
            align: 'center',
        }
    ];

    const data: DataType[] = [
        {
            key: '1',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '2',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '3',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '4',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '5',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '6',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '7',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '8',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '9',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '10',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '11',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
        {
            key: '12',
            accountName: 'Công ty CP Airsoft',
            accountNumber: '888888',
            bank: 'BIDV',
            fundManagement: 'PVcap',
            openingBalance: 10000000000,
            currentBalance: 110000000,
            investmentProduct: 'Tiền gửi có kỳ hạn',
            startTime: '01/01/2024',
            endingTime: '31/12/2024',
            profitRate: '10%',
            profit: 1000000000
        },
    ];


    return (
        <Card title='Quản lý danh mục đầu tư'>
            <Row
                gutter={[0, 16]}
                className='border-rounded'
            >
                <Col
                    span={24}
                    className='header'
                >
                    <Form
                        className='w-100'
                        layout='vertical'
                    >
                        <Row gutter={16}>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='accountName'
                                    label='Tên tài khoản'
                                >
                                    <Input
                                        placeholder='Nhập tên tài khoản'
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='accountNumber'
                                    label='Số tài khoản'
                                >
                                    <Input
                                        placeholder='Nhập số tài khoản'
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                xs={24}
                                md={8}
                                lg={8}
                            >
                                <Form.Item
                                    name='createdAt'
                                    label='Ngày tạo'
                                >
                                    <RangePicker
                                        className='w-100'
                                        format={dateFormat}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row
                            justify='end'
                            gutter={16}
                        >
                            <Col>
                                <Button
                                    danger
                                    type='primary'
                                    htmlType='submit'
                                    icon={<SearchOutlined/>}
                                >
                                    Truy vấn
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    icon={<ReloadOutlined/>}
                                >
                                    Thiết lập lại
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    icon={<FileExcelOutlined/>}
                                >
                                    Xuất file
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                    <Divider/>
                </Col>
                <Col
                    span={24}
                    className='content'
                >
                    <Table columns={columns}
                           dataSource={data}
                           scroll={{x: '100%'}}
                           rowKey={'id'}
                           pagination={{
                               pageSizeOptions: ['10', '20', '50', '100'],
                               showSizeChanger: true,
                               defaultPageSize: 10,
                               showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`, // Hiển thị tổng số record
                           }}
                    />
                </Col>
            </Row>
        </Card>
    )
}

