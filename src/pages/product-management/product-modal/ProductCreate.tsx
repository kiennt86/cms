import { Button, Col, Form, Input, notification, Row, Select } from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { useCreateProductMutation } from '../../../app/services/apiProductService.ts'


interface IProps {
  onCancel: () => void
  fetchData: () => void
}

interface ProductCreate {
  code: string,
  category: string,
  categoryName: string,
  supplier: string,
  name: string
  tier: string
}

export const ProductCreate = ({ onCancel, fetchData }: IProps) => {
  const [productCreate, { isSuccess }] = useCreateProductMutation()
  const [form] = Form.useForm()
  const handleCreate = async (value: ProductCreate) => {
    const body: BodyInterface = {
      apiCode: ApiCode.PROD_CREATE,
      requestId: REQUEST_ID,
      params: {
        code: value.code,
        category: value.category,
        categoryName: value.categoryName,
        supplier: value.supplier,
        name: value.name,
        tier: value.tier
      }
    }
    const result: any = await productCreate(body)
    if (result?.data?.msgRepose === null) {
      notification.success({ message: 'Tạo mới sản phẩm thành công' })
      onCancel()
      form.resetFields()
      fetchData()
    } else if (result?.data?.msgRepose == 'product exits.') {
      notification.error({ message: 'Sản phẩm đã tồn tại' })
    } else {
      notification.error({ message: 'Tạo mới sản phẩm thất bại' })
    }
  }

  return (
    <Form
      name='basic'
      onFinish={handleCreate}
      autoComplete='off'
      layout='vertical'
      form={form}
    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã sản phẩm'
            name='code'
            rules={[{ required: true, message: 'Hãy nhập mã sản phẩm!' }]}
          >
            <Input
              placeholder='Nhập mã sản phẩm'
            />
          </Form.Item>
          <Form.Item
            label='Danh mục'
            name='category'
            rules={[{ required: true, message: 'Hãy nhập danh mục!' }]}
          >
            <Input
              placeholder='Nhập danh mục'
            />
          </Form.Item>

          <Form.Item
            label='Tên danh mục'
            name='categoryName'
            rules={[
              {
                required: true,
                message: 'Hãy nhập tên danh mục!'
              }
            ]}>
            <Input
              placeholder='Nhập tên danh mục'
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Nhà cung cấp'
            name='supplier'
            rules={[{ required: true, message: 'Hãy nhập nhà cung cấp!' }]}
          >
            <Input
              placeholder='Nhập nhà cung cấp'
            />
          </Form.Item>
          <Form.Item
            label='Tên sản phẩm'
            name='name'
            rules={[{ required: true, message: 'Hãy nhập tên sản phẩm!' }]}
          >
            <Input
              placeholder='Nhập tên sản phẩm'
            />
          </Form.Item>
          <Form.Item
            label='Phương thức tính lãi suất'
            name='tier'
            rules={[{ required: true, message: 'Hãy nhập phương thức tính lãi suất!' }]}
          >
            <Select
              placeholder='Chọn phương thức tính lãi suất'
              options={
                [{ value: 'SINGLE', label: 'SINGLE' },
                  { value: 'BAND', label: 'BAND' },
                  { value: 'LEVEL', label: 'LEVEL' }
                ]}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
        <Col>
          <Button
            type='primary'
            htmlType='submit'
            danger
          >
            Tạo mới
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

