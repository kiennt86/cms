import { Button, notification, Typography } from 'antd'
import { ApiCode, BodyInterface } from '../../../app/types/api.ts'
import { REQUEST_ID } from '../../../app/constants/appConstant.ts'
import { ProductInterface } from '../../../app/types/product.ts'
import { useDeleteProductMutation } from '../../../app/services/apiProductService.ts'

const { Text } = Typography

type DeleteConfirmModalType = {
  fetchData: () => void
  onCancel: () => void
  record: ProductInterface
}

export const ProductDelete = ({
                                onCancel,
                                record,
                                fetchData
                              }: DeleteConfirmModalType) => {

  const [deleteProduct] = useDeleteProductMutation()

  const onDelete = async () => {
    const body: BodyInterface = {
      apiCode: ApiCode.PROD_DELETE,
      requestId: REQUEST_ID,
      params: {
        code: record.code
      }
    }
    const result: any = await deleteProduct(body)
    if (result?.data?.msgRepose === null) {
      notification.success({ message: 'Xóa sản phẩm thành công' })
      onCancel()
      fetchData()
    } else if (result?.data?.msgRepose === 'Item not exits.') {
      notification.error({ message: 'Sản phẩm không tồn tại' })
    } else {
      notification.error({ message: 'Xóa sản phẩm thất bại' })
    }
  }

  return (
    <>
      <Text style={{ textAlign: 'center' }}>
        Bạn có chắc chắn muốn xoá sản phẩm này không?
      </Text>
      <div style={{ textAlign: 'end', marginTop: '8px' }}>
        <Button
          className='mr-10'
          onClick={() => {
            onCancel()
          }}
        >
          Hủy
        </Button>
        <Button
          type='primary'
          onClick={onDelete}
          danger
        >
          Xóa
        </Button>
      </div>
    </>
  )
}
