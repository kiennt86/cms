import { Modal } from 'antd'
import { viewMode } from '../../../app/types/api.ts'
import { ProductInterface } from '../../../app/types/product.ts'
import { ProductCreate } from './ProductCreate.tsx'
import { ProductDelete } from './ProductDelete.tsx'


interface IProps {
  mode: viewMode
  visible: boolean
  record: ProductInterface
  onCancel: () => void
  fetchData: () => void
}

export const ProductModal = ({
                            onCancel,
                            visible,
                            fetchData,
                            record,
                            mode,
                            ...props
                          }: IProps) => {

  const render = () => {
    switch (mode) {
      case viewMode.create:
        return (
          <ProductCreate
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
      case viewMode.delete:
        return (
          <ProductDelete
            record={record}
            onCancel={onCancel}
            fetchData={fetchData}
          />
        )
    }
  }

  return (
    <Modal
      title={mode === viewMode.create ? 'Tạo mới sản phẩm' : mode === viewMode.update ? 'Cập nhật sản phẩm' : 'Xoá sản phẩm'}
      open={visible}
      footer={false}
      onCancel={onCancel}
      centered
      destroyOnClose={true}
      width={
        mode === viewMode.update ||
        mode === viewMode.create ||
        mode === viewMode.view
          ? 800 : 600
      }
      maskClosable={false}
      mask
      {...props}
    >
      {render()}
    </Modal>
  )
}
