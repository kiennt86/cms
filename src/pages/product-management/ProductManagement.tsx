import {
  PlusOutlined,
  SearchOutlined,
  EditOutlined,
  DeleteOutlined, FileExcelOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Table, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useMemo, useState } from 'react'
import { ApiCode, viewMode } from '../../app/types/api.ts'
import { REQUEST_ID } from '../../app/constants/appConstant.ts'
import { ProductInterface } from '../../app/types/product.ts'
import { useGetListProductsMutation } from '../../app/services/apiProductService.ts'
import { TermModal } from '../term-management/term-modal/TermModal.tsx'
import { ProductModal } from './product-modal/ProductModal.tsx'

export const ProductManagement = () => {
  const { Text } = Typography
  const [getListProductsMutation] = useGetListProductsMutation()
  const [productData, setProductData] = useState<ProductInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<ProductInterface>()

  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  const fetchData = async () => {
    try {
      const body = {
        apiCode: ApiCode.PROD_GET,
        requestId: REQUEST_ID,
        params: {}
      }

      const products: any = await getListProductsMutation(body)
      setProductData(products?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  const productList = useMemo(() => {
    if (productData && productData?.length > 0) {
      return productData?.map((item: ProductInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [productData])


  const handleModal = (action: viewMode, record?: ProductInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }


  const columns: ColumnsType<ProductInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã sản phẩm',
      dataIndex: 'code',
      key: 'code',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên sản phẩm',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }

    },
    {
      title: 'Nhà cung cấp',
      dataIndex: 'supplier',
      key: 'supplier',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Danh mục',
      key: 'category',
      dataIndex: 'category',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên danh mục',
      key: 'categoryName',
      dataIndex: 'categoryName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Phương thức tính lãi suất',
      key: 'tier',
      dataIndex: 'tier',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tác vụ',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (value, record) => (
        <Row
          gutter={5}
          justify='center'
        >
          <Col>
            <Tooltip
              placement='top'
              title='Xóa'
            >
              <Button
                type='primary'
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  handleModal(viewMode.delete, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
        </Row>
      )
    }
  ]

  return (
    <Card title='Quản lý sản phẩm'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
        >
          <Button
            type='primary'
            danger
            icon={<PlusOutlined />}
            onClick={() => {
              handleModal(viewMode.create)
            }}
          >
            Thêm mới
          </Button>
        </Col>
        <Divider />
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={productList}
                 scroll={{ x: '100%' }}
                 rowKey={'no'}
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
      <ProductModal
        mode={mode}
        visible={visible}
        onCancel={onCancel}
        record={selectedRecord}
        fetchData={fetchData}
      />
    </Card>
  )
}

