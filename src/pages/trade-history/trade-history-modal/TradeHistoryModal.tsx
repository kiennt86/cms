import { Modal } from 'antd'
import { viewMode } from '../../../app/types/api.ts'
import { TradeHistoryView } from './TradeHistoryView.tsx'
import { TradeHistoryInterface } from '../../../app/types/tradeHistory.ts'


interface IProps {
  mode: viewMode
  visible: boolean
  record: TradeHistoryInterface
  onCancel: () => void
}

export const TradeHistoryModal = ({
                                    onCancel,
                                    visible,
                                    record,
                                    mode,
                                    ...props
                                  }: IProps) => {

  const render = () => {
    switch (mode) {
      case viewMode.view:
        return (
          <TradeHistoryView
            onCancel={onCancel}
            record={record}
          />
        )
    }
  }

  return (
    <Modal
      title='Xem chi tiết lịch sử giao dịch'
      open={visible}
      footer={false}
      onCancel={onCancel}
      centered
      destroyOnClose={true}
      width={1000}
      maskClosable={false}
      mask
      {...props}
    >
      {render()}
    </Modal>
  )
}
