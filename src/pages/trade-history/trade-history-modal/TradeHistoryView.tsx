import { Button, Col, Form, Input, Row } from 'antd'
import { useEffect } from 'react'
import dayjs from 'dayjs'
import { TradeHistoryInterface } from '../../../app/types/tradeHistory.ts'
import { getStatusText } from '../../../app/constants/helperFunctions.ts'

type IType = {
  record: TradeHistoryInterface
  onCancel: () => void
}

export const TradeHistoryView = ({
                                   onCancel,
                                   record
                                 }: IType) => {

  const [form] = Form.useForm()
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'

  useEffect(() => {
    if (record) {
      form.setFieldsValue({
        merchantCode: record.merchantCode ? record.merchantCode : '-',
        amount: record.amount ? record.amount : '-',
        fee: record.fee ? record.fee : '-',
        creationDate: record.creationDate ? dayjs(record.creationDate).format(dateFormat) : '-',
        errorCode: record.errorCode ? record.errorCode : '-',
        errorDescription: record.errorDescription ? record.errorDescription : '-',
        receiveAccount: record.receiveAccount ? record.receiveAccount : '-',
        receiveBank: record.receiveBank ? record.receiveBank : '-',
        receiveName: record.receiveName ? record.receiveName : '-',
        senderAccount: record.senderAccount ? record.senderAccount : '-',
        transId: record.transId ? record.transId : '-',
        transDate: record.transDate ? dayjs(record.transDate).format(dateFormat) : '-',
        transactionType: record.transactionType ? record.transactionType : '-',
        transferDate: record.transferDate ? dayjs(record.transferDate).format(dateFormat) : '-',
        transferRequestId: record.transferRequestId ? record.transferRequestId : '-',
        vaAccount: record.vaAccount ? record.vaAccount : '-',
        status: record.status ? getStatusText(record.status) : '-',
        note: record.note ? record.note : '-'
      })
    }
  }, [record])

  return (
    <Form
      layout='vertical'
      form={form}

    >
      <Row gutter={16} className='w-full'>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Mã merchant'
            name='merchantCode'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Giá trị'
            name='amount'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Phí'
            name='fee'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Thời gian tạo'
            name='creationDate'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Mã lỗi'
            name='errorCode'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Mô tả lỗi'
            name='errorDescription'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tài khoản nhận'
            name='receiveAccount'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Ngân hàng nhận'
            name='receiveBank'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tên người nhận'
            name='receiveName'
          >
            <Input
              readOnly
            />
          </Form.Item>
        </Col>
        <Col
          xs={24}
          md={12}
          lg={12}
        >
          <Form.Item
            label='Tài khoản người gửi'
            name='senderAccount'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='ID giao dịch'
            name='transId'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Thời gian giao dịch'
            name='transDate'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Loại giao dịch'
            name='transactionType'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Ngày chuyển'
            name='transferDate'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='ID yêu cầu chuyển'
            name='transferRequestId'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Tài khoản ảo'
            name='vaAccount'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Trạng thái'
            name='status'
          >
            <Input
              readOnly
            />
          </Form.Item>
          <Form.Item
            label='Ghi chú'
            name='note'
          >
            <Input
              readOnly
            />
          </Form.Item>
        </Col>
      </Row>
      <Row className='justify-end'>
        <Col className='mr-10'>
          <Button onClick={() => onCancel()}>
            Hủy
          </Button>
        </Col>
      </Row>
    </Form>
  )
}
