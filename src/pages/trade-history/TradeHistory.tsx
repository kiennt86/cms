import {
  PlusOutlined,
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
  EyeOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col, DatePicker,
  Divider,
  Form,
  Input,
  Row, Select,
  Table, Tag, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useMemo, useState } from 'react'
import dayjs from 'dayjs'
import { ApiCode, viewMode } from '../../app/types/api.ts'
import { REQUEST_ID } from '../../app/constants/appConstant.ts'
import { getStatusText, getTagColor } from '../../app/constants/helperFunctions.ts'
import { useGetTradeHistoryMutation } from '../../app/services/apiTradeHistoryService.ts'
import { TradeHistoryInterface, TradeHistorySearchParams } from '../../app/types/tradeHistory.ts'
import { useGetListMerchantsMutation } from '../../app/services/apiMerchantService.ts'
import { MerchantInterface } from '../../app/types/merchant.ts'
import { ItemModal } from '../item-management/item-modal/ItemModal.tsx'
import { TradeHistoryModal } from './trade-history-modal/TradeHistoryModal.tsx'

export const TradeHistory = () => {
  const { Text } = Typography
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'
  const [getTradeHistoryListMutation] = useGetTradeHistoryMutation()
  const [tradeHistoryData, setTradeHistoryData] = useState<TradeHistoryInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<TradeHistoryInterface>()
  const [form] = Form.useForm<TradeHistorySearchParams>()
  const [getMerchantListMutation] = useGetListMerchantsMutation()
  const [merchantData, setMerchantData] = useState<MerchantInterface[]>([])

  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  async function fetchData(payload: TradeHistorySearchParams) {
    try {
      const body = {
        apiCode: ApiCode.TRADE_HISTORY,
        requestId: REQUEST_ID,
        params: payload
      }
      const tradeHistory: any = await getTradeHistoryListMutation(body)
      setTradeHistoryData(tradeHistory?.data?.data?.content)
    } catch (error) {
      console.error(error)
    }
  }

  async function fetchDataMerchant() {
    try {
      const body = {
        apiCode: ApiCode.MERCHANT_LIST,
        requestId: REQUEST_ID,
        params: {}
      }
      const merchants: any = await getMerchantListMutation(body)
      setMerchantData(merchants?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchDataMerchant()
  }, [])

  useEffect(() => {
    if (merchantData.length === 0) return
    form.setFieldValue('merchantCode', merchantData[6]?.code)
    form.submit()
  }, [merchantData])

  const tradeHistoryList = useMemo(() => {
    if (tradeHistoryData && tradeHistoryData?.length > 0) {
      return tradeHistoryData?.map((item: TradeHistoryInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [tradeHistoryData])

  const getMerchantList = () => {
    if (merchantData && merchantData.length > 0) {
      return merchantData.map((item: MerchantInterface) => ({
        value: item.code,
        label: item.name
      }))
    } else return []
  }

  const handleModal = (action: viewMode, record?: TradeHistoryInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }

  const handleSearch = (values: TradeHistorySearchParams) => {
    const query = {
      ...values,
      merchantCode: values.merchantCode?.trim(),
      sourceAccount: values.sourceAccount === '' ? undefined : values.sourceAccount?.trim(),
      inOut: values.inOut === '' ? undefined : values.inOut?.trim()
    }
    const newQuery: TradeHistorySearchParams = { merchantCode: '' }
    for (const key in query) {
      if (query[key] && query[key] !== undefined && query[key] !== null) {
        newQuery[key] = query[key]
      }
    }
    fetchData(newQuery)
  }

  const columns: ColumnsType<TradeHistoryInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã merchant',
      dataIndex: 'merchantCode',
      key: 'merchantCode',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Giá trị',
      dataIndex: 'amount',
      key: 'amount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Phí',
      dataIndex: 'fee',
      key: 'fee',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tài khoản nhận',
      key: 'receiveAccount',
      dataIndex: 'legalType',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Ngân hàng nhận',
      key: 'receiveBank',
      dataIndex: 'receiveBank',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên người nhận',
      key: 'receiveName',
      dataIndex: 'receiveName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tài khoản chuyển',
      key: 'senderAccount',
      dataIndex: 'senderAccount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tên người chuyển',
      key: 'senderName',
      dataIndex: 'senderName',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'Trạng thái',
      key: 'status',
      dataIndex: 'status',
      align: 'center',
      render: (record: boolean | string | number) => (
        <Tag
          color={getTagColor(record)}
          className='text-capitalize'
        >
          {getStatusText(record)}
        </Tag>
      )
    },
    {
      title: 'Tác vụ',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (value, record) => (
        <Row
          gutter={5}
          justify='center'
        >
          <Col>
            <Tooltip
              placement='top'
              title='Xem'
            >
              <Button
                icon={<EyeOutlined />}
                onClick={() => {
                  handleModal(viewMode.view, record)
                }}
              >
              </Button>
            </Tooltip>
          </Col>
        </Row>
      )
    }
  ]

  return (
    <Card title='Lịch sử giao dịch'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
          className='header'
        >
          <Form
            className='w-100'
            layout='vertical'
            onFinish={handleSearch}
            form={form}
            autoComplete='off'
          >
            <Row gutter={16}>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='merchantCode'
                  label='Mã merchant'
                >
                  <Select
                    allowClear
                    options={getMerchantList()}
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='sourceAccount'
                  label='Tài khoản nguồn'
                >
                  <Input
                    allowClear
                    placeholder='Hãy nhập tài khoản nguồn'
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='inOut'
                  label='Nạp/Rút'
                >
                  <Select
                    allowClear
                    placeholder='Chọn nạp/rút'
                    options={[
                      { value: 'IN', label: 'Nạp' },
                      { value: 'OUT', label: 'Rút' }
                    ]}
                  />
                </Form.Item>
              </Col>
              <Col className='mt-30'>
                <Button
                  danger
                  type='primary'
                  htmlType='submit'
                  icon={<SearchOutlined />}
                >
                  Truy vấn
                </Button>
              </Col>
            </Row>
          </Form>
          <Divider />
        </Col>
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={tradeHistoryList}
                 scroll={{ x: '100%' }}
                 rowKey='no'
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
      <TradeHistoryModal
        mode={mode}
        visible={visible}
        onCancel={onCancel}
        record={selectedRecord}
      />
    </Card>
  )
}

