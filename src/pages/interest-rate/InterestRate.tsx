import {
  SearchOutlined,
  EyeOutlined
} from '@ant-design/icons'
import {
  Button, Card,
  Col,
  Divider,
  Form,
  Input,
  Row, Select,
  Table, Tag, Tooltip, Typography
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useMemo, useState } from 'react'
import dayjs from 'dayjs'
import { ApiCode, viewMode } from '../../app/types/api.ts'
import { REQUEST_ID } from '../../app/constants/appConstant.ts'
import { getStatusText, getTagColor } from '../../app/constants/helperFunctions.ts'
import { useGetTradeHistoryMutation } from '../../app/services/apiTradeHistoryService.ts'
import { TradeHistoryInterface, TradeHistorySearchParams } from '../../app/types/tradeHistory.ts'
import { useGetListMerchantsMutation } from '../../app/services/apiMerchantService.ts'
import { MerchantInterface } from '../../app/types/merchant.ts'
import { useGetInterestRateMutation } from '../../app/services/apiInterestRateService.ts'
import { InterestRateInterface, InterestRateSearchParams } from '../../app/types/interestRate.ts'

export const InterestRate = () => {
  const { Text } = Typography
  const dateFormat = 'YYYY/MM/DD HH:mm:ss'
  const [getInterestRateListMutation] = useGetInterestRateMutation()
  const [interestRateData, setInterestRateDataData] = useState<InterestRateInterface[]>([])
  const [mode, setMode] = useState<viewMode>(viewMode.create)
  const [visible, setVisible] = useState<boolean>(false)
  const [selectedRecord, setSelectedRecord] = useState<InterestRateInterface>()
  const [form] = Form.useForm<InterestRateSearchParams>()
  const [getMerchantListMutation] = useGetListMerchantsMutation()
  const [merchantData, setMerchantData] = useState<MerchantInterface[]>([])

  const Nodata = () => <Text>{'-'}</Text>

  const CellContent = ({ content }: { content: string }) => (
    <Text>{content}</Text>
  )

  async function fetchData(payload: InterestRateSearchParams) {
    try {
      const body = {
        apiCode: ApiCode.INTEREST_RATE,
        requestId: REQUEST_ID,
        params: payload
      }
      const interestRate: any = await getInterestRateListMutation(body)
      setInterestRateDataData(interestRate?.data?.data?.content)
    } catch (error) {
      console.error(error)
    }
  }

  async function fetchDataMerchant() {
    try {
      const body = {
        apiCode: ApiCode.MERCHANT_LIST,
        requestId: REQUEST_ID,
        params: {}
      }
      const merchants: any = await getMerchantListMutation(body)
      setMerchantData(merchants?.data?.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    fetchDataMerchant()
  }, [])

  useEffect(() => {
    if (merchantData.length === 0) return
    form.setFieldValue('merchantCode', merchantData[6]?.code)
    form.submit()
  }, [merchantData])

  const interestRateList = useMemo(() => {
    if (interestRateData && interestRateData?.length > 0) {
      return interestRateData?.map((item: InterestRateInterface, index: number) => ({
        no: index + 1,
        ...item
      }))
    } else return []
  }, [interestRateData])

  const getMerchantList = () => {
    if (merchantData && merchantData.length > 0) {
      return merchantData.map((item: MerchantInterface) => ({
        value: item.code,
        label: item.name
      }))
    } else return []
  }

  const handleModal = (action: viewMode, record?: InterestRateInterface) => {
    setVisible(true)
    if (record) setSelectedRecord(record)
    setMode(action)
  }
  const onCancel = () => {
    setVisible(false)
  }

  const handleSearch = (values: InterestRateSearchParams) => {
    const query = {
      ...values,
      merchantCode: values.merchantCode?.trim(),
      sourceAccount: values.sourceAccount === '' ? undefined : values.sourceAccount?.trim()
    }
    const newQuery: InterestRateSearchParams = { merchantCode: '' }
    for (const key in query) {
      if (query[key] && query[key] !== undefined && query[key] !== null) {
        newQuery[key] = query[key]
      }
    }
    fetchData(newQuery)
  }

  const columns: ColumnsType<InterestRateInterface> = [
    {
      title: 'STT',
      dataIndex: 'no',
      key: 'no',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'ID giao dịch',
      dataIndex: 'tradeId',
      key: 'tradeId',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tài khoản đích',
      dataIndex: 'destinationAccount',
      key: 'destinationAccount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }

    },
    {
      title: 'Tài khoản',
      dataIndex: 'account',
      key: 'account',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'ID mặt hàng',
      key: 'itemId',
      dataIndex: 'itemId',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã mặt hàng',
      key: 'itemCode',
      dataIndex: 'itemCode',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Số lượng tnx',
      key: 'tnxAmount',
      dataIndex: 'tnxAmount',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Loại',
      key: 'type',
      dataIndex: 'type',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mã merchant',
      key: 'merchantCode',
      dataIndex: 'merchantCode',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Tài gian',
      key: 'datetime',
      dataIndex: 'datetime',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={dayjs(record).format(dateFormat)} /> : <Nodata />
      }
    },
    {
      title: 'ID kênh',
      key: 'channelId',
      dataIndex: 'channelId',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    },
    {
      title: 'Mô tả',
      key: 'description',
      dataIndex: 'description',
      align: 'center',
      render: (record) => {
        return record ? <CellContent content={record} /> : <Nodata />
      }
    }
  ]

  return (
    <Card title='Tỷ lệ lãi suất'>
      <Row
        gutter={[0, 16]}
        className='border-rounded'
      >
        <Col
          span={24}
          className='header'
        >
          <Form
            className='w-100'
            layout='vertical'
            onFinish={handleSearch}
            form={form}
            autoComplete='off'
          >
            <Row gutter={16}>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='merchantCode'
                  label='Mã merchant'
                >
                  <Select
                    allowClear
                    options={getMerchantList()}
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='sourceAccount'
                  label='Tài khoản nguồn'
                >
                  <Input
                    allowClear
                    placeholder='Hãy nhập tài khoản nguồn'
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                md={8}
                lg={6}
              >
                <Form.Item
                  name='destinationAccount'
                  label='Tài khoản đích'
                >
                  <Input
                    allowClear
                    placeholder='Hãy nhập tài khoản đích'
                  />
                </Form.Item>
              </Col>
              <Col className='mt-30'>
                <Button
                  danger
                  type='primary'
                  htmlType='submit'
                  icon={<SearchOutlined />}
                >
                  Truy vấn
                </Button>
              </Col>
            </Row>
          </Form>
          <Divider />
        </Col>
        <Col
          span={24}
          className='content'
        >
          <Table columns={columns}
                 dataSource={interestRateList}
                 scroll={{ x: '100%' }}
                 rowKey='no'
                 pagination={{
                   pageSizeOptions: ['10', '20', '50', '100'],
                   showSizeChanger: true,
                   defaultPageSize: 10,
                   showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` // Hiển thị tổng số record
                 }}
          />
        </Col>
      </Row>
    </Card>
  )
}

