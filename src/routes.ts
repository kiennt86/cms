import { createBrowserRouter } from 'react-router-dom'
import { createElement } from 'react'
import Login from './pages/auth/login/Login.tsx'
import NotFound from './pages/auth/not-found/NotFound.tsx'
import AuthLayout from './components/layout/AuthLayout.tsx'
import { AUTHENTICATION } from './app/constants/routersConstant.ts'
import ForgotPassword from './pages/auth/forgot-password/ForgotPassword.tsx'
import {
  MerchantTransactionManagement
} from './pages/merchant-transaction-management/MerchantTransactionManagement.tsx'
import { CashFlowManagement } from './pages/cashflow-management/CashFlowManagement.tsx'
import { InvestmentManagement } from './pages/investment-management/InvestmentManagement.tsx'
import { UserList } from './pages/system-params/user-list/UserList.tsx'
import { MerchantList } from './pages/system-params/merchant-list/MerchantList.tsx'
import { CustomerList } from './pages/system-params/customer-list/CustomerList.tsx'
import { TermManagement } from './pages/term-management/TermManagement.tsx'
import { ItemManagement } from './pages/item-management/ItemManagement.tsx'
import { ProductManagement } from './pages/product-management/ProductManagement.tsx'
import AppLayout from './components/layout/AppLayout.tsx'
import { TradeHistory } from './pages/trade-history/TradeHistory.tsx'
import { InterestRate } from './pages/interest-rate/InterestRate.tsx'

const authentication = {
  path: '/',
  element: createElement(AuthLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: AUTHENTICATION + '/login',
      element: createElement(Login)
    }
  ]
}

const systemParamsManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/system-params/merchants',
      element: createElement(MerchantList)
    },
    {
      path: '/system-params/users',
      element: createElement(UserList)
    },
    {
      path: '/system-params/customers',
      element: createElement(CustomerList)
    }
  ]
}

const merchantTransactionManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/merchant-transaction-management',
      element: createElement(MerchantTransactionManagement)
    }
  ]
}


const cashFlowManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/cash-flow-management',
      element: createElement(CashFlowManagement)
    }
  ]
}

const investmentManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/investment-management',
      element: createElement(InvestmentManagement)
    }
  ]
}

const termManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/term-management',
      element: createElement(TermManagement)
    }
  ]
}

const itemManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/item-management',
      element: createElement(ItemManagement)
    }
  ]
}

const productManagement = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/product-management',
      element: createElement(ProductManagement)
    }
  ]
}


const tradeHistory = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/trade-history',
      element: createElement(TradeHistory)
    }
  ]
}

const interestRate = {
  path: '/',
  element: createElement(AppLayout),
  errorElement: createElement(NotFound),
  children: [
    {
      path: '/interest-rate',
      element: createElement(InterestRate)
    }
  ]
}
export const cmsRoutes = createBrowserRouter([
  authentication,
  systemParamsManagement,
  merchantTransactionManagement,
  cashFlowManagement,
  investmentManagement,
  termManagement,
  productManagement,
  itemManagement,
  tradeHistory,
  interestRate
])