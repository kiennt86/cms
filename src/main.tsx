import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { RouterProvider } from 'react-router-dom'
import { cmsRoutes } from './routes.ts'
import { EventEmitter } from 'ahooks/es/useEventEmitter'
import './App.scss'
import './index.css'
import { store } from './app/store.ts'
import { Provider } from 'react-redux'
import { GoogleOAuthProvider } from '@react-oauth/google'


window.events = new EventEmitter()
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <GoogleOAuthProvider clientId={import.meta.env.VITE_CLIENT_ID}>
      <Provider store={store}>
        <RouterProvider router={cmsRoutes} />
      </Provider>
    </GoogleOAuthProvider>
  </React.StrictMode>
)
