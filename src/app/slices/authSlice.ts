import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {appName} from "../constants/appConstant.ts";
import {IUserProfile} from "../types/auth.ts";

const name = 'authSlice'

export interface IAuthState {
    accessToken: string | null
    profile: IUserProfile
}

const initialState: IAuthState = {
    accessToken: localStorage.getItem(appName) || null,
    profile: {
        email: '',
        email_verified: false,
        family_name: '',
        given_name: '',
        locale: '',
        name: '',
        picture: '',
        sub: ''
    }
}

const authSlice = createSlice({
    name: name,
    initialState: initialState,
    reducers: {
        logout: () => {
            localStorage.removeItem(appName)
            return {
                ...initialState,
                accessToken: null
            }
        },
        getProfile: (state, action: PayloadAction<IUserProfile>) => {
            state.profile = action.payload
        }
    }
})

export const {logout, getProfile} = authSlice.actions
export default authSlice.reducer
