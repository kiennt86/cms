import { IUserLogin } from '../types/auth'
import { apiService } from './apiService'

export const authService = apiService.injectEndpoints({
    endpoints: (build) => ({
        login: build.mutation({
            query: (userLogin: IUserLogin) => ({
                url: '/login',
                method: 'POST',
                body: userLogin
            })
        })
    })
})

export const {
    useLoginMutation,
} = authService
