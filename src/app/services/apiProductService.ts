import { apiService } from './apiService'
import {BodyInterface} from "../types/api.ts";

export const productService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getListProducts: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    createProduct: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    deleteProduct: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    })
  })
})

export const {
  useGetListProductsMutation,
  useCreateProductMutation,
  useDeleteProductMutation
} = productService
