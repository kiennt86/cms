import { apiService } from './apiService'
import { BodyInterface } from '../types/api.ts'

export const userService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getListUsers: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    }),
    createUser: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    }),
    updateUser: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    }),
    deleteUser: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    })
  })
})

export const {
  useGetListUsersMutation,
  useCreateUserMutation,
  useUpdateUserMutation,
  useDeleteUserMutation
} = userService
