import { apiService } from './apiService'
import {BodyInterface} from "../types/api.ts";

export const termService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getListTerms: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    createTerm: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    updateTerm: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    deleteTerm: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    })
  })
})

export const {
  useGetListTermsMutation,
  useCreateTermMutation,
  useUpdateTermMutation,
  useDeleteTermMutation
} = termService
