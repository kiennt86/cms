import { apiService } from './apiService'
import { BodyInterface } from '../types/api.ts'

export const customerService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getCustomer: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    })
  })
})

export const {
  useGetCustomerMutation
} = customerService
