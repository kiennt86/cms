import { apiService } from './apiService'
import { BodyInterface } from '../types/api.ts'

export const interestRateService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getInterestRate: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    })
  })
})

export const {
  useGetInterestRateMutation
} = interestRateService
