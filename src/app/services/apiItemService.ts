import { apiService } from './apiService'
import {BodyInterface} from "../types/api.ts";

export const itemService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getListItems: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    createItem: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    deleteItem: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
    updateItem: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      }),
    }),
  })
})

export const {
  useGetListItemsMutation,
  useCreateItemMutation,
  useDeleteItemMutation,
  useUpdateItemMutation
} = itemService
