import { apiService } from './apiService'
import { BodyInterface } from '../types/api.ts'

export const tradeHistoryService = apiService.injectEndpoints({
  endpoints: (build) => ({
    getTradeHistory: build.mutation({
      query: (body: BodyInterface) => ({
        url: '/',
        method: 'POST',
        body: body
      })
    })
  })
})

export const {
  useGetTradeHistoryMutation
} = tradeHistoryService
