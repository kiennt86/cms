import { apiService } from './apiService'
import {BodyInterface} from "../types/api.ts";

export const merchantService = apiService.injectEndpoints({
    endpoints: (build) => ({
        getListMerchants: build.mutation({
            query: (body: BodyInterface) => ({
                url: '/',
                method: 'POST',
                body: body
            }),
        }),
        createMerchant: build.mutation({
            query: (body: BodyInterface) => ({
                url: '/',
                method: 'POST',
                body: body
            }),
        }),
        updateMerchant: build.mutation({
            query: (body: BodyInterface) => ({
                url: '/',
                method: 'POST',
                body: body
            }),
        }),
        deleteMerchant: build.mutation({
            query: (body: BodyInterface) => ({
                url: '/',
                method: 'POST',
                body: body
            }),
        })
    })
})

export const {
    useGetListMerchantsMutation, useCreateMerchantMutation, useUpdateMerchantMutation, useDeleteMerchantMutation
} = merchantService
