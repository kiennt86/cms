import { notification } from 'antd'
import { BaseQueryFn, FetchArgs, fetchBaseQuery, FetchBaseQueryError } from '@reduxjs/toolkit/query'
import { hideLoading, showLoading } from '../constants/events.ts'

const openNotification = (
  message: string,
  messageType?: 'warning' | 'info' | 'success'
) => {
  if (messageType) {
    switch (messageType) {
      case 'warning':
        break
      case 'info':
        break
      default:
        notification.success({
          message: `${message}`,
          placement: 'top'
        })
        break
    }
  } else {
    notification.error({
      message: `${message}`,
      placement: 'top',
      style: { width: 400 }
    })
  }
}

const baseQuery = fetchBaseQuery({
  baseUrl: import.meta.env.VITE_API_URL,
  prepareHeaders: (headers) => {
    return headers
  }
})
export const fetchBaseQueryCustom: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  const isLoading = (args as any)?.params?.isLoading
  isLoading !== false && showLoading()
  const result: any = await baseQuery(args, api, extraOptions)
  hideLoading()
  if (result?.data?.msgRepose !== null) {
    openNotification(result?.data?.msgRepose)
  }
  return result
}

