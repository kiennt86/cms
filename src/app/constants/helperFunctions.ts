export const getTagColor = (status: boolean | string | number) => {
  switch (status) {
    case 'ACTIVE':
      return 'success'
    case 'INACTIVE':
      return 'error'
    case 1:
      return 'success'
    case 0:
      return 'error'
    case '1':
      return 'success'
    case '0':
      return 'error'
    case status:
      return 'success'
    case !status:
      return 'error'
  }
}

export const getStatusText = (status?: boolean | string | number) => {
  switch (status) {
    case 'ACTIVE':
      return 'Hoạt động'
    case 'INACTIVE':
      return 'Khóa'
    case 1:
      return 'Hoạt động'
    case 0:
      return 'Khóa'
    case '1':
      return 'Hoạt động'
    case '0':
      return 'Khóa'
    case status:
      return 'Hoạt động'
    case !status:
      return 'Khóa'
  }
}