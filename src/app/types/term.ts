export interface TermInterface {
  no?: number
  id: number
  code: string | null
  title: string | null
  type: string | null
  desc: string | null
  status: number
  timeCreate: string | null
  updateTime: string | null
}

export interface TermSearchParams {
  code?: string
}