export enum viewMode {
  'view' = 'view',
  'create' = 'create',
  'update' = 'update',
  'delete' = 'delete'
}

export interface MerchantInterface {
  no?: number
  id: string
  name: string | null
  agentCode: string | null
  legalType: string | null
  phone: string | null
  registrationDate: string | null
  active: number
  bankAccount: string | null
  partnerConfigId: number
  extra1: string | null
  extra2: number | null
  tid: string | null
  mid: string | null
  taxcode: string | null
  code: string | null
  dipAccount: MerchantDipAccountDetail[]
}

export interface MerchantDipAccountDetail {
  id: number
  uuid: string
  accountName: string | null
  userId: number
}

export interface MerchantSearchParams {
  taxcode?: string
  name?: string
  agentCode?: string
  time?: string
  fromDate?: string
  toDate?: string
}