export interface UserInterface {
  no?: number
  id: number
  firstname: string | null
  lastname: string | null
  phone: string | null
  pass: string
  status: string | null
  timeCreate: string | null
  updateTime: string | null
  email: string
}

export interface UserSearchParams {
  name?: string
  fromDate?: string
  toDate?: string
  time?: string
}