export interface InterestRateInterface {
  no: number | null
  id: number
  tradeId: string
  destinationAccount: string | null
  account: string
  itemId: string
  itemCode: string
  tnxAmount: string
  type: string
  merchantCode: string
  datetime: string
  channelId: string | null
  description: string | null
}

export interface InterestRateSearchParams {
  merchantCode: string
  tradeType?: string
  sourceAccount?: string
  destinationAccount?: string
  fromDate?: string
  toDate?: string
}