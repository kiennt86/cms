export interface TradeHistoryInterface {
  no: number | null
  id: number
  merchantCode: string
  amount: string
  fee: string | null
  creationDate: string | null
  errorCode: string | null
  errorDescription: string | null
  note: string | null
  receiveAccount: string | null
  receiveBank: string | null
  receiveName: string | null
  senderAccount: string | null
  senderName: string | null
  status: string
  transDate: string | null
  transId: string | null
  transactionType: string | null
  transferDate: string | null
  transferRequestId: string | null
  vaAccount: string | null
}

export interface TradeHistorySearchParams {
  merchantCode: string
  tradeType?: string
  sourceAccount?: string
  inOut?: string
}