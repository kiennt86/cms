export interface ProductInterface {
  no?: number
  code: string | null
  name: string | null
  supplier: string | null
  category: string | null
  tier: string | null
  categoryName: string | null
}

