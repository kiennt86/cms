export interface ItemInterface {
  no: number | null
  itemId: string
  itemCode: string | null
  itemName: string | null
  itemAccountNumber: string | null
  itemOwnerAccountName: string | null
  itemSupplier: string | null
  itemCategory: string | null
  itemCategoryName: string | null
  itemValue: number | null
  itemStatus: string | null
  itemInterestRate: string | number | null
  itemTier: string | null
  itemTxnDate: string | null
  itemIssuer: string | null
}