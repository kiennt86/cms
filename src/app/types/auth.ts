export interface IUserLogin {
    email: string
    password: string
}

export interface IUserProfile {
    email: string
    email_verified: boolean
    family_name: string
    given_name: string
    locale: string
    name: string
    picture: string
    sub: string
}