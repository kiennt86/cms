export enum viewMode {
  'view' = 'view',
  'create' = 'create',
  'update' = 'update',
  'delete' = 'delete'
}

export interface BodyInterface {
  apiCode: string,
  requestId: string,
  params: object
}

export enum ApiCode {
  'USER_GET' = 'USER_GET',
  'USER_CREATE' = 'USER_CREATE',
  'USER_UPDATE' = 'USER_UPDATE',
  'USER_DELETE' = 'USER_DELETE',

  'MERCHANT_GET' = 'MERCHANT_GET',
  'MERCHANT_CREATE' = 'MERCHANT_CREATE',
  'MERCHANT_UPDATE' = 'MERCHANT_UPDATE',
  'MERCHANT_DELETE' = 'MERCHANT_DELETE',
  'MERCHANT_LIST' = 'MERCHANT_LIST',

  'PARTNER_CREATE' = 'PARTNER_CREATE',
  'PARTNER_UPDATE' = 'PARTNER_UPDATE',
  'PARTNER_GET' = 'PARTNER_GET',
  'PARTNER_DELETE' = 'PARTNER_DELETE',

  'GROUP_CREATE' = 'GROUP_CREATE',
  'GROUP_UPDATE' = 'GROUP_CREATE',
  'GROUP_GET' = 'GROUP_CREATE',
  'GROUP_DELETE' = 'GROUP_CREATE',

  'PROD_GET' = 'PROD_GET',
  'PROD_CREATE' = 'PROD_CREATE',
  'PROD_DELETE' = 'PROD_DELETE',

  'ITEM_GET' = 'ITEM_GET',
  'ITEM_CREATE' = 'ITEM_CREATE',
  'ITEM_DELETE' = 'ITEM_DELETE',
  'ITEM_UPDATE' = 'ITEM_UPDATE',

  'TRADE_HISTORY' = 'TRADE_HISTORY',

  'USER_GROUP_CREATE' = 'USER_GROUP_CREATE',
  'USER_GROUP_UPDATE' = 'USER_GROUP_UPDATE',
  'USER_GROUP_GET' = 'USER_GROUP_GET',
  'USER_GROUP_DELETE' = 'USER_GROUP_DELETE',

  'TRANSFER_CONFIG_CREATE' = 'TRANSFER_CONFIG_CREATE',
  'TRANSFER_CONFIG_UPDATE' = 'TRANSFER_CONFIG_UPDATE',
  'TRANSFER_CONFIG_GET' = 'TRANSFER_CONFIG_GET',
  'TRANSFER_CONFIG_DELETE' = 'TRANSFER_CONFIG_DELETE',

  'CUSTOMER_CREATE' = 'CUSTOMER_CREATE',
  'CUSTOMER_UPDATE' = 'CUSTOMER_UPDATE',
  'CUSTOMER_GET' = 'CUSTOMER_GET',
  'CUSTOMER_DELETE' = 'CUSTOMER_DELETE',

  'TERMS_CREATE' = 'TERMS_CREATE',
  'TERMS_UPDATE' = 'TERMS_UPDATE',
  'TERMS_GET' = 'TERMS_GET',
  'TERMS_DELETE' = 'TERMS_DELETE',

  'TRANSFER_GET' = 'TRANSFER_GET',
  'INTEREST_RATE' = 'INTEREST_RATE',

  'ACCOUNT_INQUIRY' = 'ACCOUNT_INQUIRY',

  'DIP_SYNC' = 'DIP_SYNC',
}