export interface CustomerInterface {
  no: number | null
  id: number
  firstname: string | null
  lastname: string | null
  cccd: string | null
  phone: string | null
  email: string | null
  address: string | null
  va: string | null
  bankAcc: string | null
  status: string | null
  partnerId: number
  custAccount: string | null
  amount: string | null
}

export interface CustomerSearchParams {
  phone?: string
}