import {
    Action,
    configureStore,
    ThunkAction
} from '@reduxjs/toolkit'
import {setupListeners} from '@reduxjs/toolkit/query'
import {TypedUseSelectorHook, useSelector} from 'react-redux'
import {apiService} from "./services/apiService.ts";
import authReducer from './slices/authSlice.ts'

export const store = configureStore({
    reducer: {
        [apiService.reducerPath]: apiService.reducer,
        auth: authReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({serializableCheck: false}).concat(apiService.middleware)
})

setupListeners(store.dispatch)

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector
