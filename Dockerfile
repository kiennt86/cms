# 1. For build React app
FROM node AS cms_fe
# Set working directory
WORKDIR /app
#
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
# Same as npm install
RUN npm ci
COPY . /app
ENV CI=true
ENV PORT=5173
FROM cms_fe AS build
RUN npm run build
